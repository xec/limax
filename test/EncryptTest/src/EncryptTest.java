import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Created by xuyuechuan on 2016/3/3.
 */
public class EncryptTest {
    public static void main(String[] args) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] b = "test".getBytes();
        md.update(b);
        byte[] key = md.digest();
        Cipher c = Cipher.getInstance("AES/ECB/NoPadding");
        c.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"));

        byte[] value;
        value = c.update("testtesttesttesttesttesttesttest".getBytes());


        Cipher d = Cipher.getInstance("AES/ECB/NoPadding");
        d.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"));

        String text = new String(d.update(value), StandardCharsets.UTF_8);
        System.out.println(text);
    }
}
