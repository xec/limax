﻿using limax.endpoint.providerendpoint;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace limax.endpoint.script
{
    public interface ScriptSender
    {
        Exception send(string s);
    }
    public interface ScriptEngineHandle
    {
        HashSet<int> getProviders();
        int action(int t, object p);
        void registerScriptSender(ScriptSender sender);
    }
}
