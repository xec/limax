#pragma once

namespace limax {
	class EndpointImpl;
	class Engine
	{
		friend class EndpointImpl;
		static ThreadPool *pool;
		static std::mutex mutex;
		static std::condition_variable_any cond;
		static std::unordered_set<EndpointImpl*> set;
		static void add(EndpointImpl* e);
		static void remove(EndpointImpl* e);
	public:
		static void open();
		static void close(Runnable done);
		static void execute(Runnable r) { pool->execute(r); }
	};
} // namespace limax {

