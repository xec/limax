#include "common.h"

namespace limax {

	Codec::~Codec() {

	}

	class Null: public Codec {
	public:
		virtual void update(int8_t c) override {
		}
		virtual void update(int8_t data[], int32_t off, int32_t len) override {
		}
		virtual void flush() override {
		}
	};

	static std::shared_ptr<Null> _null(new Null());
	std::shared_ptr<Codec> Codec::Null() {
		return _null;
	}

	BufferedSink::BufferedSink(std::shared_ptr<Codec> _sink) :
		sink(_sink), pos(0) {
	}

	void BufferedSink::flushInternal() {
		if (pos > 0) {
			sink->update(buffer, 0, pos);
			pos = 0;
		}
	}

	void BufferedSink::update(int8_t c) {
		if (capacity == pos) {
			flushInternal();
		}
		buffer[pos++] = c;
	}

	void BufferedSink::update(int8_t data[], int32_t off, int32_t len) {
		if (len >= capacity) {
			flushInternal();
			sink->update(data, off, len);
			return;
		}
		if (len > capacity - pos) {
			flushInternal();
		}
		memmove(buffer + pos, data + off, len);
		pos += len;
	}

	void BufferedSink::flush() {
		flushInternal();
		sink->flush();
	}

	SinkStream::SinkStream(std::shared_ptr<std::ostream> _os) :
		os(_os) {
	}

	void SinkStream::update(int8_t c) {
		os->put(c);
	}

	void SinkStream::update(int8_t data[], int32_t off, int32_t len) {
		os->write(reinterpret_cast<const char*>(data + off), len);
	}

	void SinkStream::flush() {
		os->flush();
	}

} // namespace limax {

