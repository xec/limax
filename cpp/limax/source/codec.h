#pragma once

namespace limax {

	class Codec {
	public:
		virtual void update(int8_t c) = 0;
		virtual void update(int8_t data[], int32_t off, int32_t len) = 0;
		virtual void flush() = 0;
		virtual ~Codec();
		static std::shared_ptr<Codec> Null();
	};

	class CodecException {
	};

	class BufferedSink: public Codec {
		std::shared_ptr<Codec> sink;
		static const int32_t capacity = 8192;
		int8_t buffer[capacity];
		int32_t pos;
		void flushInternal();
	public:
		BufferedSink(std::shared_ptr<Codec> _sink);
		virtual void update(int8_t c) override;
		virtual void update(int8_t data[], int32_t off, int32_t len) override;
		virtual void flush() override;
	};

	class SinkStream: public Codec {
		std::shared_ptr<std::ostream> os;
	public:
		SinkStream(std::shared_ptr<std::ostream> os);
		virtual void update(int8_t c) override;
		virtual void update(int8_t data[], int32_t off, int32_t len) override;
		virtual void flush() override;
	};

} // namespace limax {

