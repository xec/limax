#pragma once

namespace limax {
	namespace helper {
		
		class OsSystemInit
		{
			OsSystemInit();
		public:
			void Startup();
			void Cleanup();
		public:
			static OsSystemInit& getInstance();
		};

	} // namespace helper {

	struct TcpClient
	{
		struct Listener
		{
			virtual ~Listener() { }
			virtual void onCreate(TcpClient *) = 0;
			virtual void onOpen(const struct sockaddr_in& local, const struct sockaddr_in& peer) = 0;
			virtual void onAbort(const struct sockaddr_in &sa) = 0;
			virtual void onRecv(const void *data, int32_t size) = 0;
			virtual void onClose(int status) = 0;
		};
		TcpClient() {}
		virtual ~TcpClient() {}
		virtual void send(const void *, int32_t) = 0;
		virtual void send(const Octets&) = 0;
		virtual void recv() = 0;
		virtual void close() = 0;
		virtual void destroy() = 0;
		static void createAsync(Listener*, std::string, short);
		static void createAsync(Listener*, const struct sockaddr_in&);
		static void createSync(Listener*, std::string, short, int contimeout, int rwtimeout);
		static void createSync(Listener*, const struct sockaddr_in&, int contimeout, int rwtimeout);
	};

} // namespace limax {

