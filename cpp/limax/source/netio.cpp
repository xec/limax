#include "common.h"

namespace limax {
	namespace helper {

		OsSystemInit& OsSystemInit::getInstance()
		{
			static OsSystemInit instance;
			return instance;
		}

		OsSystemInit::OsSystemInit()
		{
#ifdef LIMAX_VS_DEBUG_MEMORY_LEAKS_DETECT
			_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
		}

#ifdef LIMAX_OS_UNIX_FAMILY
		void OsSystemInit::Startup() {}
		void OsSystemInit::Cleanup() {}

		typedef int SOCKET;
		inline void closesocket(int s)
		{
			::shutdown(s, SHUT_RDWR);
			::close(s);
		}

		inline int WSAGetLastError()
		{
			return errno;
		}
#endif

#ifdef LIMAX_OS_WINDOWS
		void OsSystemInit::Startup()
		{
			WSADATA data;
			WSAStartup(MAKEWORD(2, 0), &data);
		}
		void OsSystemInit::Cleanup()
		{
			::WSACleanup();
		}

		typedef int32_t socklen_t;
#endif
		class AbstractTcpClient : public TcpClient
		{
		protected:
			SOCKET sock;
			struct sockaddr_in local, peer;
			Listener* listener;
			std::atomic<bool> closed;
			std::atomic<bool> close_notified;
		public:
			AbstractTcpClient(Listener* _listener, const struct sockaddr_in& _peer)
				: sock(::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)), peer(_peer), listener(_listener), closed(false), close_notified(false)
			{
				int optval = 1;
				::setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (const char *)&optval, sizeof(optval));
				::setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const char *)&optval, sizeof(optval));
				listener->onCreate(this);
			}
			virtual ~AbstractTcpClient() {}
		protected:
			void _close()
			{
				close();
				if (close_notified.exchange(true))
					return;
				listener->onClose(WSAGetLastError());
			}
			void _connected()
			{
				if (closed.load())
					return;
				listener->onOpen(local, peer);
			}
			void _abort()
			{
				close();
				if (close_notified.exchange(true))
					return;
				listener->onAbort(peer);
			}
		};


		class AsyncTcpClient : public AbstractTcpClient
		{
			BlockingQueue<Octets> sq;
			JoinableRunnable r_task;
			JoinableRunnable w_task;
		public:
			virtual ~AsyncTcpClient() {}
			AsyncTcpClient(Listener* _listener, const struct sockaddr_in& _peer)
				: AbstractTcpClient(_listener, _peer),
				r_task([this]()
			{
				if (::connect(sock, (struct sockaddr*)&peer, sizeof(peer)))
				{
					_abort();
					return;
				}
				socklen_t len;
				::getsockname(sock, (struct sockaddr *)&local, &len);
				_connected();
				char buff[8192];
				while (true)
				{
					int32_t nrecv = (int32_t)::recv(sock, buff, sizeof(buff), 0);
					if (nrecv <= 0)
					{
						_close();
						return;
					}
					listener->onRecv(buff, nrecv);
				}
			}),
				w_task([this]()
			{
				while (true)
				{
					Octets data = sq.get();
					int32_t l = (int32_t)data.size();
					if (l == 0)
						return;
					const char *p = (const char *)data.begin();
					while (true)
					{
						int32_t nsend = (int32_t)::send(sock, p, l, 0);
						if (nsend <= 0)
						{
							_close();
							return;
						}
						if (nsend == l)
							break;
						p += nsend;
						l -= nsend;
					}
				}
			})
			{
				Engine::execute(r_task);
				Engine::execute(w_task);
			}
		public:
			virtual void send(const void* data, int32_t size) override
			{
				send(Octets(data, size));
			}
			virtual void send(const Octets& o) override
			{
				sq.put(o);
			}
			virtual void recv() override {}
			virtual void close() override
			{
				if (closed.exchange(true))
					return;
				closesocket(sock);
				send(Octets());
			}
			virtual void destroy() override
			{
				close();
				r_task.join();
				w_task.join();
				delete this;
			}
		};

		class SyncTcpClient : public AbstractTcpClient
		{
		public:
			virtual ~SyncTcpClient() {}
			SyncTcpClient(Listener* _listener, const struct sockaddr_in& _peer, int contimeout, int rwtimeout)
				: AbstractTcpClient(_listener, _peer)
			{
				setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&rwtimeout, sizeof(rwtimeout));
				setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (const char*)&rwtimeout, sizeof(rwtimeout));

				{
					std::mutex mutex;
					std::condition_variable_any cond;
					volatile bool started = false;
					volatile bool timeouted = false;
					int conreturn;
					std::thread timeoutthread([&mutex, &cond, &started, &timeouted, this, contimeout]()
					{
						std::cv_status returnstatus;
						{
							std::lock_guard<std::mutex> l(mutex);
							started = true;
							cond.notify_one();
							returnstatus = cond.wait_for(mutex, std::chrono::milliseconds(contimeout));
						}
						if (std::cv_status::timeout == returnstatus) 
						{
							timeouted = true;
							close();
						}
					});
					{
						std::lock_guard<std::mutex> l(mutex);
						while (!started)
							cond.wait(mutex);
						conreturn = ::connect(sock, (struct sockaddr*)&peer, sizeof(peer));
						cond.notify_one();
					}
					timeoutthread.join();
					if (timeouted || conreturn)
					{
						_abort();
						return;
					}
				}
				socklen_t len;
				::getsockname(sock, (struct sockaddr *)&local, &len);
				_connected();
			}
		public:
			virtual void send(const void* data, int32_t size) override
			{
				if (closed.load())
					return;
				const char *p = (const char *)data;
				while (true)
				{
					int32_t nsend = (int32_t)::send(sock, p, size, 0);
					if (nsend <= 0)
					{
						_close();
						return;
					}
					if (nsend == size)
						break;
					p += nsend;
					size -= nsend;
				}
			}
			virtual void send(const Octets& o) override
			{
				send(o.begin(), (int32_t)o.size());
			}
			virtual void recv() override
			{
				if (closed.load())
					return;
				char buff[8192];
				int32_t nrecv = (int32_t)::recv(sock, buff, sizeof(buff), 0);
				if (nrecv <= 0)
				{
					_close();
					return;
				}
				listener->onRecv(buff, nrecv);
			}
			virtual void close() override
			{
				if (closed.exchange(true))
					return;
				closesocket(sock);
			}
			virtual void destroy() override
			{
				close();
				delete this;
			}
		};

	} // namespace helper {

	void TcpClient::createAsync(Listener* l, std::string ip, short port)
	{
		struct sockaddr_in peer;
		memset(&peer, 0, sizeof(peer));
		peer.sin_family = AF_INET;
		peer.sin_addr.s_addr = inet_addr(ip.c_str());
		peer.sin_port = htons(port);
		createAsync(l, peer);
	}
	void TcpClient::createAsync(Listener* l, const struct sockaddr_in& peer)
	{
		new helper::AsyncTcpClient(l, peer);
	}

	void TcpClient::createSync(Listener* l, std::string ip, short port, int contimeout, int rwtimeout)
	{
		struct sockaddr_in peer;
		memset(&peer, 0, sizeof(peer));
		peer.sin_family = AF_INET;
		peer.sin_addr.s_addr = inet_addr(ip.c_str());
		peer.sin_port = htons(port);
		createSync(l, peer, contimeout, rwtimeout);
	}
	void TcpClient::createSync(Listener* l, const struct sockaddr_in& peer, int contimeout, int rwtimeout)
	{
		new helper::SyncTcpClient(l, peer, contimeout, rwtimeout);
	}

} // namespace limax {

