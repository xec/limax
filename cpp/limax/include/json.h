#pragma once

namespace limax {

	template<class Char>
	class JSON
	{
	private:
		struct _Object{
			virtual ~_Object(){}
		};
	public:
		typedef typename std::shared_ptr<_Object> Object;
		typedef typename std::basic_string<Char> string;
	private:
		struct _String : public _Object, public string
		{
			_String() {}
			_String(const char *p) : string(p) {}
			_String(const string& p) : string(p) {}
		};
	public:
		typedef typename std::shared_ptr<_String> String;
	private:
		struct StringHash
		{
			size_t operator()(const String& a) const { return std::hash<string>()(*a); }
		};
		struct StringEqual
		{
			bool operator()(const String& a, const String& b) const { return *a == *b; }
		};
		struct _Map : public _Object, public std::unordered_map < String, Object, StringHash, StringEqual >
		{
		};
	public:
		typedef typename std::shared_ptr<_Map> Map;
	private:
		class _List : public _Object, public std::vector < Object > {};
		Object data;
	public:
		typedef typename std::shared_ptr<_List> List;
		struct Exception{};
	public:
		static const Object Null;
		static const Object True;
		static const Object False;
		JSON(Object _data) : data(_data){}
	private:
		template<class R> R cast() const
		{
			if (R r = std::dynamic_pointer_cast<typename R::element_type>(data))
				return r;
			throw Exception();
		}
		const char *ascii_string(size_t &l) const
		{
			string&s = *cast<String>();
			l = s.length();
			if (l == 0)
				throw Exception();
			char *data = new char[l + 1];
			for (size_t i = 0; i < l; i++)
			{
				if ((data[i] = s[i]) != s[i])
					throw Exception();
			}
			data[l] = 0;
			return data;
		}
		string make_string(const char *p) const
		{
			string s;
			size_t l = strlen(p);
			for (size_t i = 0; i < l; i++)
				s.push_back(p[i]);
			return s;
		}
	public:
		std::shared_ptr<JSON<Char>> get(const string& key) const
		{
			Map map = cast<Map>();
			auto it = map->find(std::make_shared<typename String::element_type>(key));
			if (it == map->end())
				throw Exception();
			return std::shared_ptr<JSON<Char>>(new JSON<Char>((*it).second));
		}
		std::shared_ptr<JSON<Char>> get(const Char *key) const { return get(string(key)); }
		std::vector<string> keySet() const
		{
			Map map = cast<Map>();
			std::vector<string> r;
			for (auto e : *map)
				r.push_back(*e.first);
			return r;
		}
		std::shared_ptr<JSON<Char>> get(size_t index) const
		{
			List list = cast<List>();
			if (index >= list->size())
				throw Exception();
			return std::shared_ptr<JSON<Char>>(new JSON<Char>((*list)[index]));
		}
		std::vector<std::shared_ptr<JSON<Char>>> toArray() const
		{
			List list = cast<List>();
			std::vector<std::shared_ptr<JSON<Char>>> r;
			for (auto e : *list)
				r.push_back(std::shared_ptr<JSON<Char>>(new JSON<Char>(e)));
			return r;
		}
		bool booleanValue() const
		{
			if (data == True) return true;
			if (data == False) return false;
			string& s = *cast<String>();
			return s.length() == 4 && towlower(s[0]) == 't' && towlower(s[1]) == 'r'&& towlower(s[2]) == 'u'&& towlower(s[3]) == 'e';
		}
		int32_t intValue() const
		{
			if (data == True) return 1;
			if (data == False) return 0;
			size_t l0;
			const char *data = ascii_string(l0);
			char *e;
			int32_t r = (int32_t)strtoul(data, &e, 10);
			size_t l1 = e - data;
			delete[]data;
			if (l0 != l1)
				throw Exception();
			return r;
		}
		int64_t longValue() const
		{
			if (data == True) return 1;
			if (data == False) return 0;
			size_t l0;
			const char *data = ascii_string(l0);
			char *e;
			int64_t r = strtoll(data, &e, 10);
			size_t l1 = e - data;
			delete[]data;
			if (l0 != l1)
				throw Exception();
			return r;
		}
		double doubleValue() const
		{
			if (data == True) return 1.0;
			if (data == False) return 0.0;
			size_t l0;
			const char *data = ascii_string(l0);
			char *e;
			double r = strtod(data, &e);
			size_t l1 = e - data;
			delete[]data;
			if (l0 != l1)
				throw Exception();
			return r;
		}
		string toString() const
		{
			if (data == Null)
				return make_string("<Null>");
			if (data == True)
				return make_string("<True>");
			if (data == False)
				return make_string("<False>");
			if (std::dynamic_pointer_cast<typename Map::element_type>(data))
				return make_string("<Object>");
			if (std::dynamic_pointer_cast<typename List::element_type>(data))
				return make_string("<List>");
			return *std::dynamic_pointer_cast<typename String::element_type>(data);
		}
		bool isNull() const
		{
			return data == Null;
		}
		static std::shared_ptr<JSON<Char>> parse(const string& text);
	};
	template<class Char> const typename JSON<Char>::Object JSON<Char>::Null;
	template<class Char> const typename JSON<Char>::Object JSON<Char>::True;
	template<class Char> const typename JSON<Char>::Object JSON<Char>::False;
#pragma endregion
#pragma region class JSONConsumer
	template <class Char>
	struct JSONConsumer
	{
		virtual ~JSONConsumer(){}
		virtual void accept(std::shared_ptr<JSON<Char>> json);
	};
#pragma endregion
#pragma region class JSONDecoder
	template <class Char>
	class JSONDecoder
	{
		typedef typename JSON<Char>::Object Object;
		typedef typename JSON<Char>::Exception Exception;
		typedef typename JSON<Char>::Map Map;
		typedef typename JSON<Char>::List List;
		typedef typename JSON<Char>::String String;
		struct _JSONValue
		{
			virtual ~_JSONValue() {}
			virtual bool accept(Char c) = 0;
			virtual void reduce(Object v) {}
		};
		std::shared_ptr<JSONConsumer<Char>> consumer;
		struct _JSONRoot : public _JSONValue
		{
			JSONDecoder &decoder;
			_JSONRoot(JSONDecoder& _decoder) : decoder(_decoder) {}
			bool accept(Char c)
			{
				if (iswspace(c))
					return true;
				if (decoder.json)
					throw Exception();
				return false;
			}
			void reduce(Object v)
			{
				if (decoder.consumer)
					decoder.consumer->accept(std::make_shared<JSON<Char>>(v));
				else
					decoder.json = std::make_shared<JSON<Char>>(v);
			}
		};
		typedef typename std::shared_ptr<_JSONRoot> JSONRoot;
		typedef typename std::shared_ptr<_JSONValue> JSONValue;
		JSONRoot root;
		JSONValue current;
		JSONValue change;
		std::shared_ptr<JSON<Char>> json;
	public:
		JSONDecoder(std::shared_ptr<JSONConsumer<Char>> _consumer) : consumer(_consumer), root(std::shared_ptr<_JSONRoot>(new _JSONRoot(*this))), current(root)
		{
		}

		JSONDecoder() : root(std::shared_ptr<_JSONRoot>(new _JSONRoot(*this))), current(root)
		{
		}
	private:
		struct _JSONObject : public _JSONValue
		{
			JSONDecoder &decoder;
			JSONValue parent;
			Map map;
			String key;
			int stage = 0;
			_JSONObject(JSONDecoder& _decoder) : decoder(_decoder), parent(_decoder.current), map(std::make_shared<typename Map::element_type>()) {}
			bool accept(Char c)
			{
				switch (stage)
				{
				case 0:
					stage = 1;
					return true;
				case 1:
					if (iswspace(c))
						return true;
					if (c == '}')
					{
						(decoder.change = parent)->reduce(map);
						return true;
					}
					return false;
				case 2:
					if (iswspace(c))
						return true;
					if (c == ':' || c == '=')
					{
						stage = 3;
						return true;
					}
					throw Exception();
				case 4:
					if (iswspace(c))
						return true;
					if (c == ',' || c == ';')
					{
						stage = 1;
						return true;
					}
					if (c == '}')
					{
						(decoder.change = parent)->reduce(map);
						return true;
					}
					throw Exception();
				}
				return iswspace(c) ? true : false;
			}
			void reduce(Object v) {
				if (stage == 1) {
					key = std::dynamic_pointer_cast<typename String::element_type>(v);
					if (!key)
						throw Exception();
					stage = 2;
				}
				else {
					map->insert(std::make_pair(key, v));
					stage = 4;
				}
			}
		};
		struct _JSONList : public _JSONValue
		{
			JSONDecoder &decoder;
			JSONValue parent;
			List list;;
			int stage = 0;
			_JSONList(JSONDecoder& _decoder) : decoder(_decoder), parent(_decoder.current), list(std::make_shared<typename List::element_type>()){}
			bool accept(Char c)
			{
				switch (stage)
				{
				case 0:
					stage = 1;
					return true;
				case 1:
					if (iswspace(c))
						return true;
					if (c == ']')
					{
						(decoder.change = parent)->reduce(list);
						return true;
					}
					return false;
				default:
					if (iswspace(c))
						return true;
					if (c == ',' || c == ';')
					{
						stage = 1;
						return true;
					}
					if (c == ']')
					{
						(decoder.change = parent)->reduce(list);
						return true;
					}
					throw Exception();
				}
			}
			void reduce(Object v)
			{
				list->push_back(v);
				stage = 2;
			}
		};
		struct _JSONString : public _JSONValue
		{
			JSONDecoder &decoder;
			JSONValue parent;
			String sb;
			int stage = 0;
			_JSONString(JSONDecoder& _decoder) : decoder(_decoder), parent(_decoder.current), sb(std::make_shared<typename String::element_type>()){}
			static int hex(Char c)
			{
				if (c >= '0' && c <= '9')
					return c - '0';
				if (c >= 'A' && c <= 'F')
					return c - 'A' + 10;
				if (c >= 'a' && c <= 'f')
					return c - 'a' + 10;
				throw Exception();
			}
			bool accept(Char c)
			{
				if (stage < 0)
				{
					stage = (stage << 4) | hex(c);
					if ((stage & 0xfff00000) == 0xfff00000)
					{
						sb->push_back((Char)stage);
						stage = 0x40000000;
					}
				}
				else if ((stage & 0x20000000) != 0)
				{
					switch (c)
					{
					case '"':
						sb->push_back('"');
						break;
					case '\\':
						sb->push_back('\\');
						break;
					case 'b':
						sb->push_back('\b');
						break;
					case 'f':
						sb->push_back('\f');
						break;
					case 'n':
						sb->push_back('\n');
						break;
					case 'r':
						sb->push_back('\r');
						break;
					case 't':
						sb->push_back('\t');
						break;
					case 'u':
						stage = -16;
						break;
					}
					stage &= ~0x20000000;
				}
				else if (c == '"')
				{
					if ((stage & 0x40000000) != 0)
						(decoder.change = parent)->reduce(sb);
					stage |= 0x40000000;
				}
				else if (c == '\\')
					stage |= 0x20000000;
				else
					sb->push_back(c);
				return true;
			}
		};
		struct _JSONNumber : public _JSONValue
		{
			JSONDecoder &decoder;
			JSONValue parent;
			String sb;
			_JSONNumber(JSONDecoder& _decoder) : decoder(_decoder), parent(_decoder.current), sb(std::make_shared<typename String::element_type>()){}
			bool accept(Char c)
			{
				switch (c)
				{
				case '+':
				case '-':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case 'E':
				case 'e':
				case '.':
					sb->push_back(c);
					return true;
				}
				(decoder.change = parent)->reduce(sb);
				return parent->accept(c);
			}
		};
		struct _JSONConst : public _JSONValue
		{
			JSONDecoder& decoder;
			JSONValue parent;
			std::vector<Char> match;
			Object value;
			int stage = 0;
			_JSONConst(JSONDecoder& _decoder, std::initializer_list<Char> _match, Object _value) : decoder(_decoder), parent(_decoder.current), match(_match), value(_value){ }
			bool accept(Char c)
			{
				if ((Char)towlower(c) != match[stage++])
					throw Exception();
				if ((size_t)stage == match.size())
					(decoder.change = parent)->reduce(value);
				return true;
			}
		};
		typedef typename std::shared_ptr<_JSONObject> JSONObject;
		typedef typename std::shared_ptr<_JSONList> JSONList;
		typedef typename std::shared_ptr<_JSONString> JSONString;
		typedef typename std::shared_ptr<_JSONNumber> JSONNumber;
		typedef typename std::shared_ptr<_JSONConst> JSONConst;
	public:
		void accept(Char c)
		{
			while (true)
			{
				bool accept = current->accept(c);
				if (change)
				{
					current = change;
					change.reset();
				}
				if (accept)
					break;
				switch (c)
				{
				case '{':
					current = std::make_shared<typename JSONObject::element_type>(*this);
					break;
				case '[':
					current = std::make_shared<typename JSONList::element_type>(*this);
					break;
				case '"':
					current = std::make_shared<typename JSONString::element_type>(*this);
					break;
				case '-':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					current = std::make_shared<typename JSONNumber::element_type>(*this);
					break;
				case 't':
				case 'T':
					current = std::shared_ptr<_JSONConst>(new _JSONConst(*this, { 't', 'r', 'u', 'e' }, JSON<Char>::True));
					break;
				case 'f':
				case 'F':
					current = std::shared_ptr<_JSONConst>(new _JSONConst(*this, { 'f', 'a', 'l', 's', 'e' }, JSON<Char>::False));
					break;
				case 'n':
				case 'N':
					current = std::shared_ptr<_JSONConst>(new _JSONConst(*this, { 'n', 'u', 'l', 'l' }, JSON<Char>::Null));
					break;
				default:
					throw Exception();
				}
			}
		}
		std::shared_ptr<JSON<Char>> get()
		{
			return json;
		}
	};

	template<class Char> inline std::shared_ptr<JSON<Char>> JSON<Char>::parse(const string& text)
	{
		JSONDecoder<Char> decoder;
		for (auto& c : text)
			decoder.accept(c);
		return decoder.get();
	}

} // namespace limax {
