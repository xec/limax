package limax.provider;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import limax.provider.ViewDataCollector.Data;
import limax.provider.providerendpoint.SyncViewToClients;

/**
 * 全局VIEW，所有客户端共享，适合做一些公用信息，比如世界消息等。
 * GlobalView不能自动同步给客户端，需要手动同步。调用syncToClient方法
 * @author xec
 *
 */
public abstract class GlobalView extends View {
	public interface CreateParameter extends View.CreateParameter {
	}

	private final static Runnable donothing = () -> {
	};
	private final Map<String, Byte> name2index = new LinkedHashMap<>();
	private final Map<Byte, String> index2name = new HashMap<>();

	protected GlobalView(CreateParameter param, String[] prefix, byte[][] collectors, String[] varnames) {
		super(param, prefix, collectors, 1);
		for (int i = 0; i < varnames.length; i++) {
			name2index.put(varnames[i], (byte) i);
			index2name.put((byte) i, varnames[i]);
		}
	}

	protected void onUpdate(String varname) {
	}

	@Override
	void hashSchedule(Runnable task) {
		getViewContext().hashSchedule(this, task);
	}

	/**
	 * 同步全局View数据给客户端
	 * @param sessionids	需要同步的客户端列表
	 * @param varname		变量名
	 */
	public final void syncToClient(Collection<Long> sessionids, String varname) {
		syncToClient(sessionids, varname, donothing);
	}

	/**
	 * 同步全局View数据给客户端
	 * @param sessionids	客户端列表
	 */
	public final void syncToClient(Collection<Long> sessionids) {
		syncToClient(sessionids, donothing);
	}

	/**
	 * 同步全局数据给特定客户端
	 * @param sessionid		客户端会话ID
	 * @param varname		变量名
	 */
	public final void syncToClient(long sessionid, String varname) {
		syncToClient(Arrays.asList(sessionid), varname);
	}

	
	/**
	 * 同步全局数据给特定客户端
	 * @param sessionid	客户端会话
	 */
	public final void syncToClient(long sessionid) {
		syncToClient(Arrays.asList(sessionid));
	}

	/**
	 * 同步全局View数据给客户端
	 * @param sessionids
	 * @param varname
	 * @param done		完成回调
	 */
	public final void syncToClient(Collection<Long> sessionids, String varname, Runnable done) {
		syncToClient(sessionids, Arrays.asList(name2index.get(varname)), done);
	}

	public final void syncToClient(Collection<Long> sessionids, Runnable done) {
		syncToClient(sessionids, name2index.values(), done);
	}

	public final void syncToClient(long sessionid, String varname, Runnable done) {
		syncToClient(Arrays.asList(sessionid), varname);
	}

	public final void syncToClient(long sessionid, Runnable done) {
		syncToClient(Arrays.asList(sessionid));
	}

	private void syncToClient(Collection<Long> sessionids, Collection<Byte> varindexes, Runnable done) {
		hashSchedule(() -> {
			try {
				if (isClosed())
					return;
				SyncViewToClients p = protocol(SyncViewToClients.DT_VIEW_DATA);
				p.sessionids.addAll(sessionids);
				varindexes.stream().flatMap(e -> vdc.binary(e, cb.get(e))).collect(() -> p.vardatas, List::add,
						List::addAll);
				if (!p.vardatas.isEmpty()) {
					if (isScriptEnabled())
						p.stringdata = prepareStringHeader(p).append(varindexes.stream()
								.flatMap(e -> vdc.string(e, cb.get(e))).collect(Collectors.joining())).toString(":P:");
					syncViewToClients(p);
				}
			} catch (Throwable t) {
				done.run();
			}
		});
	}

	@Override
	void onUpdate(byte varindex, Data data) {
		onUpdate(index2name.get(varindex));
	}

	@Override
	void close() {
		hashSchedule(() -> super.close());
	}
}
