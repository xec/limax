package limax.provider;

import limax.net.ServerManager;

public interface ProviderManager extends ServerManager {
	void close(long sessionid);
}
