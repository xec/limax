package limax.codec;

import limax.util.Helper;

/**
 * Created by xuyuechuan on 2016/3/7.
 */
public class TestOctets {
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static void main(String[] args) throws MarshalException {

        String text = "0000002980c70000006401000000000d000000000000000000000101ff34000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000784932733a49303a49303a49303a503f353f4c3f663f4c3f623f49303a3f633f49303a3f643f49303a3a3f673f4c3f623f49303a3f633f49303a3f643f49303a3f653f49303a3a3f683f4c3f623f49303a3f633f49303a3f643f49303a3a3f693f4c3f623f49303a3f633f49303a3f643f49303a3a3a3a503a0000002980d80000006401000000000d000000000200000001020001000000000d00000001ff34000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080804932733a49323a49313a49323a503a5049336c75707a343a3f383f4c3f663f4c3f623f49303a3f633f49303a3f643f49303a3a3f673f4c3f623f49303a3f633f49303a3f643f49303a3f653f49303a3a3f683f4c3f623f49303a3f633f49303a3f643f49303a3a3f693f4c3f623f49303a3f633f49303a3f643f49303a3a3a3a";
        byte[] data = hexStringToByteArray(text);

        OctetsStream ocs = new OctetsStream();

        ocs.insert(ocs.size(), data);

        int offset = 0;

        while (true) {

            final int type = ocs.unmarshal_int();
            final int size = ocs.unmarshal_size();
            offset += size;
            ocs.position(offset);

        }

//        String str = Helper.toHexString(ocs.getBytes());
//        System.out.println(ocs);
    }
}
