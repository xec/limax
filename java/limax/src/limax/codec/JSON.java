package limax.codec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JSON {
	private final Object data;

	public static final Object Null = new Object() {
		@Override
		public String toString() {
			return "null";
		}
	};

	public static final Object True = new Object() {
		@Override
		public String toString() {
			return "true";
		}
	};

	public static final Object False = new Object() {
		@Override
		public String toString() {
			return "false";
		}
	};

	public static class Exception extends java.lang.Exception {
		private static final long serialVersionUID = 7067191163493197089L;

		Exception(String message, Throwable e) {
			super(message, e);
		}

		Exception(Throwable e) {
			super(e);
		}
	}

	JSON(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return data.toString();
	}

	@SuppressWarnings("unchecked")
	public JSON get(String key) throws Exception {
		try {
			return new JSON(((Map<String, Object>) data).get(key));
		} catch (Throwable t) {
			throw new Exception(t);
		}
	}

	@SuppressWarnings("unchecked")
	public Set<String> keySet() throws Exception {
		try {
			return ((Map<String, Object>) data).keySet();
		} catch (Throwable t) {
			throw new Exception(t);
		}
	}

	@SuppressWarnings("unchecked")
	public JSON get(int index) throws Exception {
		try {
			return new JSON(((List<Object>) data).get(index));
		} catch (Throwable t) {
			throw new Exception(t);
		}
	}

	@SuppressWarnings("unchecked")
	public JSON[] toArray() throws Exception {
		try {
			List<JSON> list = new ArrayList<JSON>();
			for (Object o : (List<Object>) data)
				list.add(new JSON(o));
			return list.toArray(new JSON[0]);
		} catch (Throwable t) {
			throw new Exception(t);
		}
	}

	public boolean booleanValue() throws Exception {
		try {
			if (data == True)
				return true;
			if (data == False)
				return false;
			return Boolean.parseBoolean((String) data);
		} catch (Throwable t) {
			throw new Exception(data + " cast to boolean", t);
		}
	}

	public int intValue() throws Exception {
		try {
			if (data == True)
				return 1;
			if (data == False)
				return 0;
			return Integer.parseInt((String) data);
		} catch (Throwable t) {
			throw new Exception(data + " cast to int", t);
		}
	}

	public long longValue() throws Exception {
		try {
			if (data == True)
				return 1L;
			if (data == False)
				return 0L;
			return Long.parseLong((String) data);
		} catch (Throwable t) {
			throw new Exception(data + " cast to long", t);
		}
	}

	public double doubleValue() throws Exception {
		try {
			if (data == True)
				return 1.0;
			if (data == False)
				return 0.0;
			return Double.parseDouble((String) data);
		} catch (Throwable t) {
			throw new Exception(data + " cast to double", t);
		}
	}

	public boolean isNull() {
		return data == Null;
	}

	public static JSON parse(String text) throws Exception {
		try {
			JSONDecoder decoder = new JSONDecoder();
			for (int i = 0, l = text.length(); i < l; i++)
				decoder.accept(text.charAt(i));
			return decoder.get();
		} catch (java.lang.Exception e) {
			throw new Exception(e);
		}
	}

	public static String stringify(Object obj) throws CodecException {
		StringBuilder sb = new StringBuilder();
		JSONEncoder.encode(obj, sb);
		return sb.toString();
	}
}
