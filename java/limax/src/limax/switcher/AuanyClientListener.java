package limax.switcher;

import limax.net.ClientListener;
import limax.net.Config;
import limax.net.Manager;
import limax.net.State;
import limax.net.StateTransport;
import limax.net.Transport;
import limax.switcher.switcherauany.Kick;
import limax.util.Trace;

public final class AuanyClientListener implements ClientListener {

	private static final AuanyClientListener instance = new AuanyClientListener();

	public static AuanyClientListener getInstance() {
		return instance;
	}

	private AuanyClientListener() {
	}

	private volatile Transport transport;

	Transport getTransport() {
		return transport;
	}

	@Override
	public void onTransportAdded(Transport transport) {
		if (Trace.isInfoEnabled())
			Trace.info("AuanyClientManager onConnected " + transport);
		State newstate = new State();
		newstate.merge(limax.switcher.states.AuanyClient.AuanyClient);
		newstate.merge(limax.switcher.states.ProviderServer.ForProvider);
		((StateTransport) transport).setState(newstate);
		ProviderListener.getInstance().auanyOnline(transport);
		this.transport = transport;
	}

	@Override
	public void onTransportRemoved(Transport transport) {
		if (Trace.isInfoEnabled())
			Trace.info("AuanyClientManager onDisconnect", transport.getCloseReason());
		this.transport = null;
		ProviderListener.getInstance().doUnBind(transport);
	}

	@Override
	public void onAbort(Transport transport) {
	}

	@Override
	public void onManagerInitialized(Manager manager, Config config) {
	}

	@Override
	public void onManagerUninitialized(Manager manager) {
	}

	public void process(Kick p) {
		Trace.fatal(p.message);
		System.exit(0);
	}
}
