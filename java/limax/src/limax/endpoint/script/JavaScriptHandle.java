package limax.endpoint.script;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;

public class JavaScriptHandle implements ScriptEngineHandle {
	private final Invocable invoker;
	private final Set<Integer> providers = new HashSet<Integer>();

	@SuppressWarnings("unchecked")
	public JavaScriptHandle(ScriptEngine engine, Reader init) throws Exception {
		engine.eval(new InputStreamReader(getClass().getResourceAsStream("map.js")));
		engine.eval(new InputStreamReader(getClass().getResourceAsStream("limax.js")));
		engine.eval(
				"function sender(obj){ limax(0, function(s) { var r = obj.send(s); return r ? r : undefined; }); }");
		engine.eval(init);
		this.invoker = (Invocable) engine;
		Object o = engine.get("providers");
		if (!(o instanceof Map))
			throw new RuntimeException("init script must set var providers = [pvid0,..];");
		for (Object v : ((Map<Integer, Object>) o).values())
			providers.add((Integer) v);
	}

	@Override
	public Set<Integer> getProviders() {
		return providers;
	}

	@Override
	public int action(int t, Object p) throws Exception {
		return (int) invoker.invokeFunction("limax", t, p);
	}

	@Override
	public void registerScriptSender(ScriptSender sender) throws Exception {
		invoker.invokeFunction("sender", sender);
	}

}
