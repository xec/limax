package limax.endpoint;

import limax.net.ClientListener;

/**
 * 连接事件监听器
 * 
 * @author xec
 *
 */
public interface EndpointListener extends ClientListener {
	/**
	 * 当Socket连接建立时回调
	 */
	void onSocketConnected();

	/**
	 * 当交换秘钥成功时回调
	 */
	void onKeyExchangeDone();

	/**
	 * 服务器存活回调
	 * @param ms 连接用时
	 */
	void onKeepAlived(int ms);

	/**
	 * 错误回调
	 * @param source
	 * @param code
	 * @param exception
	 */
	void onErrorOccured(int source, int code, Throwable exception);
}
