package limax.endpoint;

import limax.endpoint.ViewContext.Type;
import limax.net.ClientManager;

/**
 * 连接点管理器，好像没啥用
 * 
 * @author xec
 *
 */
public interface EndpointManager extends ClientManager {

	/**
	 * 得到自己的session id
	 * @return sessionid
	 */
	long getSessionId();

	long getAccountFlags();

	ViewContext getViewContext(int pvid, Type type);
}
