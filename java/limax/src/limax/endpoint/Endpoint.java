package limax.endpoint;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import limax.codec.CharSink;
import limax.codec.JSON;
import limax.codec.JSONDecoder;
import limax.defines.ErrorSource;
import limax.defines.SessionType;
import limax.net.Engine;
import limax.util.ConcurrentEnvironment;
import limax.util.Dispatcher.Dispatchable;
import limax.util.HttpClient;

/**
 * 表示本地的网络Endpoint，可以理解为本地的网络连接终端。
 * 
 * @author xec
 *
 */
public final class Endpoint {

	private Endpoint() {
	}

	/**
	 * 打开客户端引擎
	 * 
	 * @param netProcessors nio网络核心？
	 * @param protocolSchedulers	协议线程？好像是指取数据的线程，即调度线程。
	 * @param applicationExecutors	应用执行线程？可能是指具体执行回调的工作线程。
	 * @throws Exception
	 */
	public static void openEngine(int netProcessors, int protocolSchedulers, int applicationExecutors)
			throws Exception {
		Engine.open(netProcessors, protocolSchedulers, applicationExecutors);
	}

	
	/**
	 * 打开客户端引擎，启动网络核心，默认1个网络核心，1个协议任务线程？4个执行线程？
	 * @throws Exception
	 */
	public static void openEngine() throws Exception {
		openEngine(1, 1, 4);
	}

	public static void closeEngine(final Runnable done) {
		AuanyService.cleanup();
		ConcurrentEnvironment.getInstance().executeStandaloneTask(new Runnable() {
			@Override
			public void run() {
				try {
					Engine.close();
				} finally {
					if (done != null)
						done.run();
				}
			}
		});
	}

	/**
	 * 构造一个登陆连接配置
	 * 
	 * @param serverIp		服务器IP
	 * @param serverPort	服务器端口(这里端口是网关的端口,也就是switcher的端口)
	 * @param username		用户名
	 * @param token			token口令，用于第三方登录
	 * @param platflag		平台信息，IOS， Android等，只是一个字符串，没啥意义
	 * @return				平台配置信息，通过调用build得到 EndpointConfig
	 */
	public static EndpointConfigBuilder createLoginConfigBuilder(String serverIp, int serverPort, String username,
			String token, String platflag) {
		final EndpointConfigBuilderImpl config = new EndpointConfigBuilderImpl(serverIp, serverPort, platflag, username,
				token, false);
		return config.isConfigCanLogin() ? config : null;
	}

	/**
	 * 通过网关群构造登陆配置
	 * @param service	网关群
	 * @param username	用户名
	 * @param token		token口令
	 * @param platflag	平台信息
	 * @return
	 */
	public static EndpointConfigBuilder createLoginConfigBuilder(ServiceInfo service, String username, String token,
			String platflag) {
		ServiceInfo.SwitcherConfig switcher = service.randomSwitcherConfig();
		return createLoginConfigBuilder(switcher.host, switcher.port, username, token, platflag);
	}

	public static EndpointConfigBuilder createPingOnlyConfigBuilder(String serverIp, int serverPort) {
		return new EndpointConfigBuilderImpl(serverIp, serverPort, "", "", "", true);
	}

	private static void mapPvidsAppendValue(Map<Integer, Byte> pvids, Integer s, int nv) {
		Byte v = pvids.get(s);
		if (null == v)
			v = Byte.valueOf((byte) nv);
		else
			v = Byte.valueOf((byte) (v | nv));
		pvids.put(s, v);
	}

	private static Collection<Integer> makeProtocolProviderIds(EndpointConfig config) {
		final Set<Integer> pvids = new HashSet<Integer>();
		for (int type : config.getEndpointState().getSizePolicy().keySet()) {
			int pvid = type >>> 8;
			if (pvid > 0)
				pvids.add(pvid);
		}
		return pvids;
	}

	private static Collection<Integer> makeScriptProviderIds(EndpointConfig config) {
		if (config.getScriptEngineHandle() != null)
			return config.getScriptEngineHandle().getProviders();
		return Collections.emptySet();
	}

	private static Map<Integer, Byte> makeProviderMap(EndpointConfig config) {
		final Map<Integer, Byte> pvids = new HashMap<Integer, Byte>();
		for (Integer s : makeProtocolProviderIds(config))
			mapPvidsAppendValue(pvids, s, SessionType.ST_PROTOCOL);
		for (Integer s : config.getStaticViewClasses().keySet())
			mapPvidsAppendValue(pvids, s, SessionType.ST_STATIC);
		for (Integer s : config.getVariantProviderIds())
			mapPvidsAppendValue(pvids, s, SessionType.ST_VARIANT);
		for (Integer s : makeScriptProviderIds(config))
			mapPvidsAppendValue(pvids, s, SessionType.ST_SCRIPT);
		mapPvidsAppendValue(pvids, AuanyService.providerId, SessionType.ST_STATIC);
		return pvids;
	}

	
	/**
	 * 开始连接服务器
	 * 
	 * @param config	连接配置参数
	 * @param listener	接收事件回调的监听器
	 */
	public static void start(final EndpointConfig config, final EndpointListener listener) {
		ConcurrentEnvironment.getInstance().executeStandaloneTask(new Runnable() {
			@Override
			public void run() {
				try {
					if (null == Engine.getProtocolScheduler())
						throw new Exception("endpoint need call openEngine");
					Map<Integer, Byte> pvids = makeProviderMap(config);
					if (!config.isPingServerOnly() && pvids.isEmpty())
						throw new Exception("endpoint no available provider");
					new EndpointManagerImpl(config, listener, pvids);
				} catch (final Exception e) {
					config.getClientManagerConfig().getDispatcher().run(new Dispatchable() {
						@Override
						public void run() {
							listener.onErrorOccured(ErrorSource.ENDPOINT, 0, e);
						}
					});
				}
			}
		});
	}

	public static List<ServiceInfo> loadServiceInfos(String httpHost, int httpPort, int appid, long timeout,
			int maxsize, File cacheDir, boolean staleEnable) throws Exception {
		JSONDecoder decoder = new JSONDecoder();
		new HttpClient("http://" + httpHost + ":" + httpPort + "/app?native=" + appid, timeout, maxsize, cacheDir,
				staleEnable).transfer(new CharSink(decoder));
		List<ServiceInfo> services = new ArrayList<ServiceInfo>();
		for (JSON json : decoder.get().get("services").toArray())
			services.add(new ServiceInfo(appid, json));
		return services;
	}

	private static AtomicReference<EndpointManagerImpl> defaultEndpointManager = new AtomicReference<EndpointManagerImpl>();

	static void setDefaultEndpointManager(EndpointManagerImpl manager) {
		defaultEndpointManager.compareAndSet(null, manager);
	}

	static void clearDefaultEndpointManager(EndpointManagerImpl manager) {
		defaultEndpointManager.compareAndSet(manager, null);
	}

	public static EndpointManager getDefaultEndpointManager() {
		return defaultEndpointManager.get();
	}
}
