package limax.endpoint;

import java.util.concurrent.Executor;

import limax.endpoint.script.ScriptEngineHandle;
import limax.net.State;

/**
 * 连接配置构造器
 * 
 * @author xec
 *
 */
public interface EndpointConfigBuilder {

	EndpointConfigBuilder inputBufferSize(int inputBufferSize);

	EndpointConfigBuilder outputBufferSize(int outputBufferSize);

	EndpointConfigBuilder executor(Executor executor);

	EndpointConfigBuilder endpointState(State... states);

	EndpointConfigBuilder staticViewClasses(View.StaticManager... managers);

	EndpointConfigBuilder variantProviderIds(int... pvids);

	EndpointConfigBuilder scriptEngineHandle(ScriptEngineHandle handle);

	/**
	 * 构造成 EndpointConfig
	 * @return
	 */
	EndpointConfig build();

}
