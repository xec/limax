package limax.zdb;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import limax.util.ConcurrentEnvironment;

public final class Lockey implements Comparable<Lockey> {
	private final int index;
	private final Object key;
	private final int hashcode;
	private volatile ReentrantReadWriteLock lock;

	private final static ThreadLocal<Map<Lockey, Integer>> rCounter = ThreadLocal.withInitial(() -> new HashMap<>());

	public Lockey(int id, Object key) {
		this.index = id;
		this.key = key;
		this.hashcode = id ^ (id << 16) ^ key.hashCode();
	}

	public Lockey alloc() {
		lock = new ReentrantReadWriteLock();
		return this;
	}

	public boolean isWriteLockedByCurrentThread() {
		return lock.isWriteLockedByCurrentThread();
	}

	public boolean isReadLockedByCurrentThread() {
		return rCounter.get().containsKey(this);
	}

	public Object getKey() {
		return key;
	}

	public void rUnlock() {
		lock.readLock().unlock();
		Integer count = rCounter.get().get(this) - 1;
		if (count == 0)
			rCounter.get().remove(this);
		else
			rCounter.get().put(this, count);
	}

	public void wUnlock() {
		lock.writeLock().unlock();
	}

	public boolean rTryLock() {
		boolean r = lock.readLock().tryLock();
		if (r) {
			Integer count = rCounter.get().get(this);
			rCounter.get().put(this, count == null ? 1 : count + 1);
		}
		return r;
	}

	public boolean wTryLock() {
		return lock.writeLock().tryLock();
	}

	public void rLock() {
		try {
			lock.readLock().lockInterruptibly();
			Integer count = rCounter.get().get(this);
			rCounter.get().put(this, count == null ? 1 : count + 1);
		} catch (InterruptedException ex) {
			if (((ConcurrentEnvironment.Worker) Thread.currentThread()).isLuciferInterrupted())
				throw new XDeadLock();
			throw new XError(toString());
		}
	}

	public void wLock() {
		try {
			lock.writeLock().lockInterruptibly();
		} catch (InterruptedException ex) {
			if (((ConcurrentEnvironment.Worker) Thread.currentThread()).isLuciferInterrupted())
				throw new XDeadLock();
			throw new XError(toString());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public int compareTo(Lockey o) {
		int x = index - o.index;
		return x != 0 ? x : ((Comparable<Object>) key).compareTo(o.key);
	}

	@Override
	public int hashCode() {
		return hashcode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj instanceof Lockey) {
			Lockey o = (Lockey) obj;
			return this.index == o.index && key.equals(o.key);
		}
		return false;
	}

	@Override
	public String toString() {
		return index + "." + key;
	}
}
