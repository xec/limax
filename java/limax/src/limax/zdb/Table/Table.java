package limax.zdb.Table;

public interface Table {
	enum Persistence {
		MEMORY, DB
	}

	String getName();

	Persistence getPersistence();
}
