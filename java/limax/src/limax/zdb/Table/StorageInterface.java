package limax.zdb.Table;

import limax.zdb.StorageBackend.StorageEngine;

public interface StorageInterface {
	StorageEngine getEngine();

	int marshalN();

	int marshal0();

	int snapshot();

	int flush();
}
