package limax.zdb.Table;

import limax.zdb.LoggerBackend.LoggerEngine;
import limax.zdb.Table.StorageInterface;

public abstract class AbstractTable implements Table {

	AbstractTable() {
	}

	public abstract StorageInterface open(limax.xmlgen.Table meta, LoggerEngine logger);

	public abstract void close();

}
