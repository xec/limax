package limax.zdb;

public interface Listener {
	default void onChanged(Object key, Object value) throws Exception {
	}

	default void onRemoved(Object key, Object value) throws Exception {
	}

	default void onChanged(Object key, Object value, String fullVarName, Note note) throws Exception {
	}
}
