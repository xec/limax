package limax.zdb.LoggerBackend;

import java.io.IOException;

public interface LoggerEngine {
	void close();

	void checkpoint();

	void backup(String path, boolean increment) throws IOException;

	void dropTables(String[] tableNames) throws Exception;
}
