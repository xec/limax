package limax.zdb.TableCache;

import java.util.Collection;

import limax.zdb.*;
import limax.zdb.TRecord.State;
import limax.zdb.Table.TStorage;
import limax.zdb.Table.TTable;

public abstract class TTableCache<K, V> {

	public interface Query<K, V> {
		void onQuery(K key, V value);
	}

	/**
	 * 
	 * On memory table callback it while threshold reached,
	 * 
	 * Other table callback never.
	 * 
	 * 
	 * @param <K>
	 *            key
	 * @param <V>
	 *            value
	 */
	public interface RemoveHandle<K, V> {
		void onRecordRemoved(K key, V value);
	}

	private volatile TTable<K, V> table;
	private volatile RemoveHandle<K, V> removedhandle;
	private volatile int capacity;

	TTableCache() {
	}

	public void initialize(TTable<K, V> table, limax.xmlgen.Table meta) {
		this.table = table;
		this.capacity = meta.getCacheCapValue();
	}

	public static <K, V> TTableCache<K, V> newInstance(TTable<K, V> table, limax.xmlgen.Table meta) {
		String cacheClass = meta.getOtherAttrs().get("cacheClass");
		cacheClass = cacheClass == null ? Zdb.meta().getDefaultTableCache() : cacheClass.trim();
		try {
			@SuppressWarnings("unchecked")
			TTableCache<K, V> theCache = (TTableCache<K, V>) Class.forName(cacheClass).newInstance();
			theCache.initialize(table, meta);
			return theCache;
		} catch (Throwable e) {
			throw new XError(e);
		}
	}

	/**
	 * Removes all of the mappings from this map. The map will be empty after
	 * this call returns. clear all record, even if dirty
	 * 
	 * @throws UnsupportedOperationException
	 *             not memory table
	 */
	public abstract void clear();

	/**
	 * cleanup memory, dirty record not clear
	 */
	public abstract void clean();

	/**
	 * @see Query
	 * @param query
	 *            the callback
	 */
	public abstract void walk(Query<K, V> query);

	public abstract int size();

	public final TTable<K, V> getTable() {
		return table;
	}

	public final int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	/**
	 * 
	 * @see RemoveHandle
	 * @param handle
	 *            the callback
	 */
	public void setRemovedhandle(RemoveHandle<K, V> handle) {
		this.removedhandle = handle;
	}

	public abstract TRecord<K, V> get(K k);

	public abstract void addNoLog(K key, TRecord<K, V> r);

	public abstract void add(K key, TRecord<K, V> r);

	public abstract TRecord<K, V> remove(K k);

	public final void _walk(Collection<TRecord<K, V>> records, Query<K, V> query) {
		records.forEach(r -> {
			Lockey lock = r.getLockey();
			lock.rLock();
			try {
				V value = r.getValue();
				if (null != value)
					query.onQuery(r.getKey(), value);
			} finally {
				lock.rUnlock();
			}
		});
	}

	public final void logAddRemove(K key, TRecord<K, V> r) {
		Transaction.currentSavepoint().add(r.getLogKey(), new Log() {
			private final State saved_state = r.state();

			@Override
			public void commit() {
				if (!r.commit())
					remove(key);
				r.getTable().onRecordChanged(r, true, saved_state);
			}

			@Override
			public void rollback() {
				remove(key);
			}
		});
	}

	public final boolean tryRemoveRecord(TRecord<K, V> r) {
		Lockey lockey = r.getLockey();
		K key = r.getKey();
		if (!lockey.wTryLock())
			return false;
		try {
			final TStorage<K, V> storage = r.getTable().getStorage();
			if (null == storage) {
				remove(key);
				if (null != removedhandle)
					Zdb.executor().execute(() -> removedhandle.onRecordRemoved(r.getKey(), r.getValue()));
				return true;
			}
			if (storage.isChangedOrUnknown(key)) {
				return false;
			}
			remove(key);
			return true;
		} finally {
			lockey.wUnlock();
		}
	}
}
