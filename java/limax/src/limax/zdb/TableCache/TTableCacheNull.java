package limax.zdb.TableCache;

import limax.zdb.TRecord;
import limax.zdb.Table.TTable;

import java.util.WeakHashMap;

public class TTableCacheNull<K, V> extends TTableCache<K, V> {
	private WeakHashMap<K, TRecord<K, V>> weakmap = new WeakHashMap<K, TRecord<K, V>>(16, 0.75f);

	@Override
	public void initialize(TTable<K, V> table, limax.xmlgen.Table meta) {
		super.initialize(table, meta);
	}

	@Override
	public void clear() {
		weakmap.clear();
	}

	@Override
	public void clean() {
	}

	@Override
	public void walk(Query<K, V> query) {
	}

	@Override
	public int size() {
		return weakmap.size();
	}

	@Override
	public TRecord<K, V> get(K k) {
		return weakmap.get(k);
	}

	@Override
	public void addNoLog(K key, TRecord<K, V> r) {
		weakmap.put(key, r);
	}

	@Override
	public void add(K key, TRecord<K, V> r) {
		weakmap.put(key, r);
		logAddRemove(key, r);
	}

	@Override
	public TRecord<K, V> remove(K k) {
		return weakmap.remove(k);
	}

}
