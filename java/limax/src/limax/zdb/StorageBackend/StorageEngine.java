package limax.zdb.StorageBackend;

import limax.codec.Octets;
import limax.zdb.IWalk;

public interface StorageEngine {
	public Octets find(Octets key);

	public boolean exist(Octets key);

	public boolean insert(Octets key, Octets value);

	public void replace(Octets key, Octets value);

	public void remove(Octets key);

	public void walk(IWalk iw);
}
