package limax.zdb;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

public interface ZdbMBean {

	void backup(String path, boolean increment) throws IOException;

	long getLuciferInterruptCount();

	long getTransactionCount();

	long getTransactionFalse();

	long getTransactionException();

	/**
	 * 
	 * @param timeout
	 *            at least 500ms, less than 500, auto adjust
	 * @throws CancellationException
	 *             if the computation was cancelled
	 * @throws ExecutionException
	 *             if the computation threw an exception
	 * @throws InterruptedException
	 *             if the current thread was interrupted while waiting
	 * @throws TimeoutException
	 *             if the wait timed out
	 */
	void testAlive(long timeout) throws InterruptedException, ExecutionException, TimeoutException;

	Map<String, AtomicInteger> top(String nsClass, String nsLock);

}
