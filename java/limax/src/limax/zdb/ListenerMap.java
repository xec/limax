package limax.zdb;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import limax.util.Trace;

public final class ListenerMap {
	private final Map<String, Set<Listener>> listenerMap = new HashMap<>();
	private final Lock sync = new ReentrantLock();

	private volatile Map<String, Set<Listener>> listenerMapCopy = new HashMap<String, Set<Listener>>();

	private void setListenerMapCopy() {
		Map<String, Set<Listener>> localCopy = new HashMap<>();
		for (Map.Entry<String, Set<Listener>> e : listenerMap.entrySet()) {
			Set<Listener> values = new HashSet<>();
			values.addAll(e.getValue());
			localCopy.put(e.getKey(), values);
		}
		this.listenerMapCopy = localCopy;
	}

	public final String add(String name, Listener l) {
		sync.lock();
		try {
			if (!listenerMap.computeIfAbsent(name, v -> new HashSet<>()).add(l))
				throw new IllegalStateException("Listener has added");
			setListenerMapCopy();
		} finally {
			sync.unlock();
		}
		return name;
	}

	public final void remove(String name, Listener l) {
		sync.lock();
		try {
			Set<Listener> ls = listenerMap.get(name);
			if (null != ls) {
				boolean changed = ls.remove(l);
				if (ls.isEmpty())
					listenerMap.remove(name);
				if (changed)
					setListenerMapCopy();
			}
		} finally {
			sync.unlock();
		}
	}

	public final boolean hasListener() {
		final Map<String, Set<Listener>> localCopy = this.listenerMapCopy;
		return false == localCopy.isEmpty();
	}

	public final boolean hasListener(String fullVarName) {
		final Map<String, Set<Listener>> localCopy = this.listenerMapCopy;
		return null != localCopy.get(fullVarName);
	}

	public final void notifyChanged(String fullVarName, Object key, Object value) {
		notify(ChangeKind.CHANGED_ALL, fullVarName, key, value, null);
	}

	public final void notifyRemoved(String fullVarName, Object key, Object value) {
		notify(ChangeKind.REMOVED, fullVarName, key, value, null);
	}

	public final void notifyChanged(String fullVarName, Object key, Object value, Note note) {
		notify(ChangeKind.CHANGED_NOTE, fullVarName, key, value, note);
	}

	public  static enum ChangeKind {
		CHANGED_ALL, CHANGED_NOTE, REMOVED
	}

	public  final void notify(ChangeKind kind, String fullVarName, Object key, Object value, Note note) {
		final Map<String, Set<Listener>> localCopy = this.listenerMapCopy;
		final Set<Listener> listeners = localCopy.get(fullVarName);
		if (null == listeners)
			return;
		for (Listener l : listeners) {
			Transaction trans = Transaction.current();
			int spBefore = trans.currentSavepointId();
			int spBeforeAccess = spBefore > 0 ? trans.getSavepoint(spBefore).getAccess() : 0;
			try {
				switch (kind) {
				case CHANGED_ALL:
					l.onChanged(key, value);
					break;
				case CHANGED_NOTE:
					l.onChanged(key, value, fullVarName, note);
					break;
				case REMOVED:
					l.onRemoved(key, value);
					break;
				}
			} catch (Throwable e) {
				if (Trace.isErrorEnabled())
					Trace.error("doChanged key=" + key + " name=" + fullVarName, e);
				int spAfter = trans.currentSavepointId();
				if (0 == spBefore) {
					if (spAfter > 0)
						trans._rollback(spBefore + 1);
				} else {
					if (spAfter < spBefore)
						throw new IllegalStateException("spAfter < spBefore");
					if (trans.getSavepoint(spBefore).isAccessSince(spBeforeAccess))
						trans._rollback(spBefore);
					else if (spAfter > spBefore)
						trans._rollback(spBefore + 1);
				}
			}
		}
	}
}
