package limax.zdb;

import limax.zdb.LoggerBackend.LoggerEdb;
import limax.zdb.LoggerBackend.LoggerEngine;
import limax.zdb.LoggerBackend.LoggerMysql;
import limax.zdb.Table.AbstractTable;
import limax.zdb.Table.StorageInterface;
import limax.zdb.Table.TTable;
import limax.zdb.Table.TableSys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class Tables {
	private final Map<String, AbstractTable> tables = new HashMap<>();
	private final ReentrantReadWriteLock flushLock = new ReentrantReadWriteLock();
	private LoggerEngine logger;

	private List<StorageInterface> storages = new ArrayList<>();
	private TableSys tableSys = new TableSys();

    public TableSys getTableSys() {
		return tableSys;
	}

    public void add(AbstractTable table) {
		if (null != tables.put(table.getName(), table))
			throw new XError("duplicate table name " + table.getName());
	}

	/**
	 * 打开数据库
	 * @param meta
	 */
	void open(limax.xmlgen.Zdb meta) {
		if (null != logger)
			throw new XError("tables opened");
		switch (meta.getEngineType()) {
		case MYSQL:
			logger = new LoggerMysql(meta);
			break;
		case EDB:
			logger = new LoggerEdb(meta);
			break;
		}
		storages.add(tableSys.open(meta.getTable(tableSys.getName()), logger));
		Map<String, Integer> map = new HashMap<>();
		AtomicInteger idAlloc = new AtomicInteger();
		for (AbstractTable table : tables.values()) {

			// 添加表到数据库?
			StorageInterface storage = table.open(meta.getTable(table.getName()), logger);
			if (storage != null)
				storages.add(storage);
			TTable<?, ?> ttable = (TTable<?, ?>) table;
			ttable.setLockId(map.computeIfAbsent(ttable.getLockName(), k -> idAlloc.incrementAndGet()));
		}

		// 添加系统表
		add(tableSys);
	}

	final void close() {
		storages.clear();
		tables.values().forEach(table -> table.close());
		if (null != logger) {
			logger.close();
			logger = null;
		}
	}

	public Lock flushReadLock() {
		return flushLock.readLock();
	}

	boolean isFlushWriteLockHeldByCurrentThread() {
		return flushLock.isWriteLockedByCurrentThread();
	}

	Lock flushWriteLock() {
		return flushLock.writeLock();
	}

	List<StorageInterface> getStorages() {
		return storages;
	}

	LoggerEngine getLogger() {
		return logger;
	}

	TTable<?, ?> getTable(String table) {
		return (TTable<?, ?>) tables.get(table);
	}
}
