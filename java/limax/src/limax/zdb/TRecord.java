package limax.zdb;

import limax.codec.MarshalException;
import limax.codec.OctetsStream;
import limax.util.Trace;
import limax.zdb.Table.TStorage;
import limax.zdb.Table.TTable;

public final class TRecord<K, V> extends XBean {
	private final TTable<K, V> table;
	private final Lockey lockey;
	private V value;
	private State state;

	private volatile long lastAccessTime = System.nanoTime();

	public void access() {
		lastAccessTime = System.nanoTime();
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public static enum State {
		INDB_GET, INDB_ADD, INDB_REMOVE, ADD, REMOVE
	}

	@Override
	public String toString() {
		return table.getName() + "," + lockey + "," + state;
	}

	public TTable<K, V> getTable() {
		return table;
	}

	@SuppressWarnings("unchecked")
	public K getKey() {
		return (K) lockey.getKey();
	}

	public  OctetsStream marshalKey() {
		return table.marshalKey(getKey());
	}

	public  OctetsStream marshalValue() {
		return table.marshalValue(value);
	}

	public State state() {
		return state;
	}

	@Override
	public void notify(LogNotify notify) {
		table.onRecordChanged(this, notify);
	}

	public boolean commit() {
		switch (state) {
		case REMOVE:
			return false;
		case INDB_GET:
			throw new XError("zdb:invalid record state");
		default:
			return true;
		}
	}

	public Lockey getLockey() {
		return lockey;
	}

	private static final String RECORD_VARNAME = "value";

	public LogKey getLogKey() {
		return new LogKey(this, RECORD_VARNAME);
	}

	public TRecord(TTable<K, V> table, V value, Lockey lockey, State state) {
		super(null, RECORD_VARNAME);
		this.table = table;
		if (null != value)
			Logs.link(value, this, RECORD_VARNAME, State.INDB_GET != state);
		this.value = value;
		this.lockey = lockey;
		this.state = state;
	}

	public  void _remove() {
		Logs.link(value, null, null);
		Transaction.currentSavepoint().addIfAbsent(getLogKey(), new LogAddRemove());
		value = null;
	}

	public boolean remove() {
		switch (state) {
		case INDB_GET:
			_remove();
			state = State.INDB_REMOVE;
			return true;
		case INDB_ADD:
			_remove();
			state = State.INDB_REMOVE;
			return true;
		case ADD:
			_remove();
			state = State.REMOVE;
			return true;
		default:
			return false;
		}
	}

	public  void _add(V value) {
		Logs.link(value, this, RECORD_VARNAME);
		Transaction.currentSavepoint().addIfAbsent(getLogKey(), new LogAddRemove());
		this.value = value;
	}

	public boolean add(V value) {
		switch (state) {
		case INDB_REMOVE:
			_add(value);
			state = State.INDB_ADD;
			return true;
		case REMOVE:
			_add(value);
			state = State.ADD;
			return true;
		default:
			return false;
		}
	}

	public V getValue() {
		return value;
	}

	public  class LogAddRemove implements Log {
		private final V saved_value;
		private final State saved_state;

		LogAddRemove() {
			saved_value = value;
			saved_state = state;
		}

		@Override
		public void commit() {
			table.onRecordChanged(TRecord.this, false, saved_state);
		}

		@Override
		public void rollback() {
			value = saved_value;
			state = saved_state;
		}

		@Override
		public String toString() {
			return "state=" + saved_state + " value=" + saved_value;
		}
	}

	private OctetsStream snapshotKey = null;
	private OctetsStream snapshotValue = null;
	private State snapshotState = null;

	public boolean tryMarshalN() {
		if (!lockey.rTryLock())
			return false;
		try {
			marshal0();
		} finally {
			lockey.rUnlock();
		}
		return true;
	}

	public void marshal0() {
		switch (state) {
		case ADD:
		case INDB_GET:
		case INDB_ADD:
			snapshotKey = marshalKey();
			snapshotValue = marshalValue();
			break;
		case INDB_REMOVE:
			snapshotKey = marshalKey();
		case REMOVE:
		}
	}

	public void snapshot() {
		switch (snapshotState = state) {
		case ADD:
		case INDB_ADD:
			state = State.INDB_GET;
			break;
		case REMOVE:
			if (Trace.isDebugEnabled())
				Trace.debug("snapshot REMOVE " + this);
		case INDB_REMOVE:
			table.getCache().remove(getKey());
		case INDB_GET:
		}
	}

	public boolean flush(TStorage<K, V> storage) {
		switch (snapshotState) {
		case INDB_ADD:
		case INDB_GET:
			storage.flushKeySize += snapshotKey.size();
			storage.flushValueSize += snapshotValue.size();
			storage.getEngine().replace(snapshotKey, snapshotValue);
			return true;
		case ADD:
			storage.flushKeySize += snapshotKey.size();
			storage.flushValueSize += snapshotValue.size();
			if (!storage.getEngine().insert(snapshotKey, snapshotValue))
				throw new XError("insert fail");
			return true;
		case INDB_REMOVE:
			storage.flushKeySize += snapshotKey.size();
			storage.getEngine().remove(snapshotKey);
			return true;
		case REMOVE:
		}
		return false;
	}

	public void clear() {
		snapshotKey = null;
		snapshotValue = null;
		snapshotState = null;
	}

	public boolean exist() {
		if (Trace.isDebugEnabled())
			Trace.debug("TRecord.exist " + this);
		switch (snapshotState) {
		case INDB_GET:
		case INDB_ADD:
		case ADD:
			return true;
		case INDB_REMOVE:
		case REMOVE:
		}
		return false;
	}

	public OctetsStream find() {
		if (Trace.isDebugEnabled())
			Trace.debug("TRecord.find " + this);
		switch (snapshotState) {
		case INDB_GET:
		case INDB_ADD:
		case ADD:
			return snapshotValue;
		case INDB_REMOVE:
		case REMOVE:
		}
		return null;
	}

	@Override
	public OctetsStream marshal(OctetsStream arg0) {
		throw new UnsupportedOperationException();
	}

	@Override
	public OctetsStream unmarshal(OctetsStream arg0) throws MarshalException {
		throw new UnsupportedOperationException();
	}

}
