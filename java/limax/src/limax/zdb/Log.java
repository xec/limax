package limax.zdb;

public interface Log {
	void commit();

	void rollback();
}
