package limax.net;

import java.net.SocketAddress;

import javax.net.ssl.SSLContext;

public interface ServerManagerConfig extends ManagerConfig {

	SocketAddress getLocalAddress();

	int getBacklog();

	int getMaxTransportCount();

	boolean isAutoListen();

	boolean isWebSocketEnabled();

	SSLContext getSSLContext();
}
