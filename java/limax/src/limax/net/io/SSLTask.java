package limax.net.io;

import java.nio.ByteBuffer;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLEngineResult.HandshakeStatus;
import javax.net.ssl.SSLEngineResult.Status;
import javax.net.ssl.SSLException;

abstract class SSLTask extends NetTaskImpl implements SSLSwitcher {
	private final SSLContext sslContext;
	private boolean sslON;
	private SSLEngine engine;
	private ByteBuffer netDataIn;
	private ByteBuffer appDataIn;
	private ByteBuffer appDataOut;

	protected SSLTask(int rsize, int wsize, NetProcessor processor, SSLContext sslContext) {
		super(rsize, wsize, processor);
		this.sslContext = sslContext;
		this.sslON = false;
	}

	private SSLEngineResult wrap() throws SSLException {
		ByteBuffer netDataOut = ByteBuffer.allocateDirect(engine.getSession().getPacketBufferSize());
		SSLEngineResult rs = engine.wrap(appDataOut, netDataOut);
		netDataOut.flip();
		super.send(netDataOut);
		return rs;
	}

	private SSLEngineResult unwrap() throws SSLException {
		while (true) {
			netDataIn.flip();
			SSLEngineResult rs = engine.unwrap(netDataIn, appDataIn);
			netDataIn.compact();
			switch (rs.getStatus()) {
			case BUFFER_OVERFLOW:
				appDataIn.flip();
				appDataIn = ByteBuffer
						.allocateDirect(appDataIn.remaining() + engine.getSession().getApplicationBufferSize())
						.put(appDataIn);
				break;
			case BUFFER_UNDERFLOW:
				if (netDataIn.capacity() < engine.getSession().getPacketBufferSize()) {
					netDataIn.flip();
					netDataIn = ByteBuffer.allocateDirect(engine.getSession().getPacketBufferSize()).put(netDataIn);
				}
			case OK:
			case CLOSED:
				return rs;
			}
		}
	}

	private void handshake(SSLEngineResult rs) throws SSLException {
		HandshakeStatus hs = rs.getHandshakeStatus();
		while (true) {
			switch (hs) {
			case NEED_TASK:
				engine.getDelegatedTask().run();
				hs = engine.getHandshakeStatus();
				break;
			case NEED_WRAP:
				rs = wrap();
				hs = rs.getHandshakeStatus();
				break;
			case NEED_UNWRAP:
				rs = unwrap();
				hs = rs.getHandshakeStatus();
				if (rs.getStatus() == Status.BUFFER_UNDERFLOW)
					return;
				break;
			case FINISHED:
				while (appDataOut.hasRemaining())
					wrap();
				return;
			default:
				break;
			}
		}
	}

	@Override
	byte[] recv() {
		if (!sslON)
			return super.recv();
		ByteBuffer rbuf = getReaderBuffer();
		synchronized (rbuf) {
			rbuf.flip();
			int len = netDataIn.position() + rbuf.remaining();
			if (len > netDataIn.capacity()) {
				netDataIn.flip();
				netDataIn = ByteBuffer.allocateDirect(len).put(netDataIn);
			}
			netDataIn.put(rbuf);
			rbuf.compact();
		}
		try {
			SSLEngineResult rs = unwrap();
			if (rs.getHandshakeStatus() == HandshakeStatus.NOT_HANDSHAKING)
				while (rs.getStatus() != Status.BUFFER_UNDERFLOW)
					rs = unwrap();
			else
				handshake(rs);
		} catch (SSLException e) {
		}
		appDataIn.flip();
		byte[] data = new byte[appDataIn.remaining()];
		appDataIn.get(data).compact();
		return data;
	}

	@Override
	public void send(ByteBuffer buffer) {
		if (!sslON) {
			super.send(buffer);
			return;
		}
		appDataOut = buffer;
		try {
			SSLEngineResult rs = wrap();
			if (rs.getHandshakeStatus() == HandshakeStatus.NOT_HANDSHAKING)
				while (appDataOut.hasRemaining())
					wrap();
			else
				handshake(rs);
		} catch (SSLException e) {
		}
	}

	@Override
	public void sendFinal() {
		if (sslON)
			detach();
		super.sendFinal();
	}

	@Override
	public void attach(String host, int port, boolean clientMode, byte[] sendBeforeHandshake) {
		if (sslON)
			return;
		engine = sslContext.createSSLEngine(host, port);
		engine.setUseClientMode(clientMode);
		netDataIn = ByteBuffer.allocateDirect(engine.getSession().getPacketBufferSize());
		appDataIn = ByteBuffer.allocateDirect(engine.getSession().getApplicationBufferSize());
		appDataOut = ByteBuffer.allocateDirect(0);
		sslON = true;
		if (sendBeforeHandshake != null)
			super.send(ByteBuffer.wrap(sendBeforeHandshake));
	}

	@Override
	public void detach() {
		if (!sslON)
			return;
		engine.closeOutbound();
		try {
			while (!engine.isOutboundDone())
				wrap();
		} catch (SSLException e) {
		}
		sslON = false;
	}
}
