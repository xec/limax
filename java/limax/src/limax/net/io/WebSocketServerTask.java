package limax.net.io;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Queue;

import javax.net.ssl.SSLContext;

import limax.codec.MarshalException;
import limax.codec.OctetsStream;
import limax.codec.SHA1;

class RFC6455Exception extends Exception {
	private static final long serialVersionUID = 4618764472702188950L;

	private final byte[] code;

	public RFC6455Exception(short code, String message) {
		super(message);
		byte[] msg = null;
		byte[] tmp = message.getBytes(StandardCharsets.UTF_8);
		msg = new byte[tmp.length + 2];
		msg[0] = (byte) ((code >> 8) & 0xFF);
		msg[1] = (byte) (code & 0xFF);
		System.arraycopy(tmp, 0, msg, 2, tmp.length);
		this.code = msg;
	}

	public byte[] getCode() {
		return code;
	}
}

class RFC6455Server {
	private static final int maxFrameSize = Integer.getInteger("limax.net.io.RFC6455Server.maxFrameSize", 65536)
			.intValue();

	public static final byte opcodeCont = 0;
	public static final byte opcodeText = 1;
	public static final byte opcodeBinary = 2;
	public static final byte opcodeClose = 8;
	public static final byte opcodePing = 9;
	public static final byte opcodePong = 10;

	public static final short closeNormal = 1000;
	public static final short closeNotSupportFrame = 1003;
	public static final short closeVolatilePolicy = 1006;
	public static final short closeSizeExceed = 1009;

	private OctetsStream os = new OctetsStream();
	private byte opcode;

	public byte[] unWrap(byte[] in) throws RFC6455Exception {
		os.insert(os.size(), in);
		os.begin();
		try {
			opcode = (byte) (os.unmarshal_byte() & 15);
			byte t = os.unmarshal_byte();
			if ((t & 0x80) == 0)
				throw new RFC6455Exception(closeVolatilePolicy, "Volatile 5.3.");
			long length = t & 127;
			if (length == 126)
				length = os.unmarshal_short() & 0xFFFFL;
			else if (length == 127)
				length = os.unmarshal_long();
			if (length < 0 || length > maxFrameSize)
				throw new RFC6455Exception(closeSizeExceed,
						"length = " + length + " buf MaxFrameSize = " + maxFrameSize);
			int len = (int) length;
			byte mask[] = new byte[] { os.unmarshal_byte(), os.unmarshal_byte(), os.unmarshal_byte(),
					os.unmarshal_byte() };
			int remain = os.remain();
			if (remain < len) {
				os.rollback();
				return null;
			}
			int pos = os.position();
			int endpos = pos + len;
			byte[] array = os.array();
			for (int i = pos, j = 0; i < endpos; i++, j = (j + 1) & 3)
				array[i] ^= mask[j];
			os.position(endpos);
			os.commit();
			return Arrays.copyOfRange(array, pos, endpos);
		} catch (MarshalException e) {
			os.rollback();
		}
		return null;
	}

	public byte getOpcode() {
		return opcode;
	}

	public static ByteBuffer wrap(byte opcode, byte[] data, int off, int len) {
		ByteBuffer bb = ByteBuffer.allocateDirect(len + 14);
		bb.put((byte) (opcode | 0x80));
		if (len <= 125) {
			bb.put((byte) len);
		} else if (len <= 65535) {
			bb.put((byte) 126);
			bb.put((byte) ((len >> 8) & 0xff));
			bb.put((byte) (len & 0xff));
		} else {
			bb.put((byte) 127);
			bb.putLong(len);
		}
		bb.put(data, off, len).flip();
		return bb;
	}
}

class WebSocketServerTask implements WebSocketTask, NetOperation, NetProcessor {
	private static final long handShakeTimeout = Long.getLong("limax.net.io.WebSocketServerTask.handShakeTimeout",
			1000);
	private static final int maxRequestSize = 16384;
	private static final byte[] zero = new byte[0];
	private final NetTaskImpl task;
	private final WebSocketProcessor processor;
	private final RFC6455Server server = new RFC6455Server();
	private byte[] request = new byte[0];
	private URI requestURI;
	private URI origin;
	private SocketAddress local;
	private SocketAddress peer;
	private byte lastOpcode;

	WebSocketServerTask(ServerContextImpl context, WebSocketProcessor processor) {
		this.processor = processor;
		this.task = new ServerTask(context, this);
	}

	WebSocketServerTask(ServerContextImpl context, WebSocketProcessor processor, SSLContext sslContext) {
		this.processor = processor;
		this.task = new SSLServerTask(context, this, sslContext);
	}

	private boolean _fillRequest(byte[] in) {
		int l = request.length;
		request = Arrays.copyOf(request, l + in.length);
		System.arraycopy(in, 0, request, l, in.length);
		l = request.length;
		return l >= 4 && request[l - 4] == 0xd && request[l - 3] == 0xa && request[l - 2] == 0xd
				&& request[l - 1] == 0xa;
	}

	private boolean fillRequest(byte[] in) {
		boolean r = _fillRequest(in);
		if (request.length > maxRequestSize)
			throw new IllegalArgumentException(
					"maxRequestSize = " + maxRequestSize + " but requestLength = " + request.length);
		return r;
	}

	private static class ParseResult {
		private final String message;
		private final boolean succeed;

		ParseResult(String message, boolean succeed) {
			this.message = message;
			this.succeed = succeed;
		}
	}

	private ParseResult parseRequest() throws Exception {
		String[] header = new String(request, StandardCharsets.UTF_8).split("(\r\n)");
		String method = header[0];
		if (!method.startsWith("GET"))
			throw new IllegalArgumentException("Invalid method <" + method + ">");
		requestURI = URI.create(method.substring(4, method.length() - 9).trim());
		if (!method.endsWith("HTTP/1.1"))
			throw new IllegalArgumentException("Invalid version <" + method.substring(method.length() - 8) + ">");
		String swkey = "";
		int check = 0;
		for (int i = 1, n = header.length; i < n; i++) {
			int pos = header[i].indexOf(":");
			String key = header[i].substring(0, pos).toLowerCase();
			String val;
			switch (key) {
			case "connection":
				val = header[i].substring(pos + 1, header[i].length()).trim();
				if (val.toLowerCase().lastIndexOf("upgrade") == -1)
					throw new IllegalArgumentException("Invalid Header<" + header[i] + ">");
				check |= 1;
				break;
			case "upgrade":
				val = header[i].substring(pos + 1, header[i].length()).trim();
				if (!val.equalsIgnoreCase("websocket"))
					throw new IllegalArgumentException("Invalid Header<" + header[i] + ">");
				check |= 2;
				break;
			case "sec-websocket-version":
				val = header[i].substring(pos + 1, header[i].length()).trim();
				if (!val.equals("13"))
					return new ParseResult("HTTP/1.1 400 Bad Request\r\nSec-WebSocket-Version: 13\r\n\r\n", false);
				check |= 4;
				break;
			case "sec-websocket-key":
				swkey = header[i].substring(pos + 1, header[i].length()).trim();
				check |= 8;
				break;
			case "origin":
				try {
					origin = URI.create(header[i].substring(pos + 1, header[i].length()).trim());
				} catch (Exception e) {
					origin = URI.create("null");
				}
				check |= 16;
				break;
			}
		}
		if (check != 31)
			throw new IllegalArgumentException("incompletion head " + check);
		return new ParseResult(
				"HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: "
						+ Base64.getEncoder().encodeToString(
								SHA1.digest((swkey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes()))
						+ "\r\n\r\n",
				true);
	}

	@Override
	public void process(byte[] in) throws Exception {
		if (request != null) {
			if (!fillRequest(in))
				return;
			try {
				ParseResult r = parseRequest();
				task.send(r.message.getBytes());
				if (r.succeed) {
					request = null;
					if (processor.setup(local, new WebSocketAddress(peer, requestURI, origin)))
						task.enableReadWrite();
					else
						task.disableReadWrite();
				} else {
					task.sendFinal();
				}
			} catch (Exception e) {
				e.printStackTrace();
				task.send("HTTP/1.1 400 Bad Request\r\n\r\n".getBytes());
				task.sendFinal();
			}
			return;
		}
		while (true) {
			try {
				byte[] data = server.unWrap(in);
				if (data == null)
					return;
				in = zero;
				byte opcode = server.getOpcode();
				if (opcode == RFC6455Server.opcodeCont)
					opcode = lastOpcode;
				else
					lastOpcode = opcode;
				switch (opcode) {
				case RFC6455Server.opcodePing:
					task.send(RFC6455Server.wrap(RFC6455Server.opcodePong, data, 0, data.length));
					break;
				case RFC6455Server.opcodeText:
					processor.process(new String(data, StandardCharsets.UTF_8));
					break;
				case RFC6455Server.opcodeBinary:
					processor.process(data);
					break;
				case RFC6455Server.opcodeClose:
					if (data.length >= 2) {
						int code = ((data[0] << 8) & 0xff) + (data[1] & 0xff);
						String reason = new String(data, 2, data.length - 2, StandardCharsets.UTF_8);
						processor.process(code, reason);
					} else
						processor.process(1000, "");
					task.sendFinal();
					return;
				case RFC6455Server.opcodePong:
					break;
				default:
					throw new RFC6455Exception(RFC6455Server.closeNotSupportFrame, "Invalid Frame opcode = " + opcode);
				}
			} catch (RFC6455Exception e) {
				sendClose(e);
				return;
			}
		}
	}

	@Override
	public void shutdown(boolean eventually, Throwable closeReason) {
		processor.shutdown(eventually, closeReason);
	}

	@Override
	public void setup(NetTask nettask) {
		processor.setup(this);
	}

	@Override
	public boolean setup(SocketAddress local, SocketAddress peer) {
		this.local = local;
		this.peer = peer;
		if (task instanceof SSLSwitcher) {
			InetSocketAddress addr = (InetSocketAddress) peer;
			((SSLSwitcher) task).attach(addr.getHostName(), addr.getPort(), false, null);
		}
		resetAlarm(handShakeTimeout);
		return true;
	}

	@Override
	public void send(String text) {
		byte[] data = text.getBytes(StandardCharsets.UTF_8);
		task.send(RFC6455Server.wrap(RFC6455Server.opcodeText, data, 0, data.length));
	}

	@Override
	public void send(byte[] binary) {
		task.send(RFC6455Server.wrap(RFC6455Server.opcodeBinary, binary, 0, binary.length));
	}

	private void sendClose(RFC6455Exception e) {
		byte[] code = e.getCode();
		task.send(RFC6455Server.wrap(RFC6455Server.opcodeClose, code, 0, code.length));
		if (task instanceof SSLSwitcher)
			((SSLSwitcher) task).detach();
		task.sendFinal();
	}

	@Override
	public void sendFinal() {
		sendClose(new RFC6455Exception(RFC6455Server.closeNormal, "Close Normal"));
	}

	@Override
	public void resetAlarm(long millisecond) {
		task.resetAlarm(millisecond);
	}

	@Override
	public ByteBuffer getReaderBuffer() {
		return task.getReaderBuffer();
	}

	@Override
	public Queue<ByteBuffer> getWriteBuffer() {
		return task.getWriteBuffer();
	}

	@Override
	public void attachKey(SelectionKey key) throws SocketException {
		task.attachKey(key);
	}

	@Override
	public void close(Throwable e) {
		task.close(e);
	}

	@Override
	public boolean isFinalInitialized() {
		return task.isFinalInitialized();
	}

	@Override
	public void setFinal() {
		task.setFinal();
	}

	@Override
	public void bytesSent(long n) {
		task.bytesSent(n);
	}

	@Override
	public void schedule() {
		task.schedule();
	}

	@Override
	public void onReadBufferEmpty() {
		task.onReadBufferEmpty();
	}

	@Override
	public void onWriteBufferEmpty() {
		task.onWriteBufferEmpty();
	}

}
