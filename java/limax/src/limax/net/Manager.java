package limax.net;

public interface Manager {
	void close();

	void close(Transport transport);

	Listener getListener();

	Config getConfig();

	Manager getWrapperManager();
}
