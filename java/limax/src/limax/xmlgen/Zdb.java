package limax.xmlgen;

import java.io.File;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.List;

import org.w3c.dom.Element;

import limax.util.ElementHelper;
import limax.util.Trace;
import limax.util.XMLUtils;

public class Zdb extends Naming {

	public enum EngineType {
		MYSQL, EDB
	}

    /**
     * DB类型，默认是ZDB，可选 jdbc
     */
    private String dbhome;
	private String trnhome;

	private String defaultTableCache = "limax.zdb.TableCache.TTableCacheLRU";
	private boolean zdbVerify = false;

	private int autoKeyInitValue = 0;
	private int autoKeyStep = 4096;

	private int corePoolSize = 30;
	private int procPoolSize = 10;
	private int schedPoolSize = 5;

	private int checkpointPeriod = 60000;
	private int luciferPeriod = 1000;
	private long snapshotFatalTime = 200;
	private int marshalPeriod = -1;
	private int marshalN = 1;

	private int jdbcPoolSize = 5;
	private int edbCacheSize = 65536;
	private int edbLoggerPages = 16384;

	private boolean enableReadLock = true;

	public static Zdb loadFromClass() throws Exception {
		// table._Meta_ not contains _sys_ meta

		// 创建表，meta为生成出来的java文件
		Method method = Class.forName("table._Meta_").getMethod("create");
		method.setAccessible(true);
		Zdb zdb = (Zdb) method.invoke(null);

		// 增加系统表, 包含 autokey
		// add _sys_ meta
		Xbean _AutoKey_ = new Xbean(zdb, "_AutoKey_");
		new Variable.Builder(_AutoKey_, "name", "string");
		new Variable.Builder(_AutoKey_, "initValue", "int");
		new Variable.Builder(_AutoKey_, "step", "int");
		new Variable.Builder(_AutoKey_, "current", "long");
		Xbean _AutoKeys_ = new Xbean(zdb, "_AutoKeys_");

		new Variable.Builder(_AutoKeys_, "autoKeys", "list").value("_AutoKey_");
		new Table.Builder(zdb, "_sys_", "string", "_AutoKeys_");
		compile(zdb);
		return zdb;
	}

    /**
     * 获得数据库引擎类型
     * @param dbhome 数据库连接字符串
     * @return MYSQL or EDB
     */
    public static EngineType getEngineType(String dbhome) {
		if (dbhome.startsWith("jdbc:mysql"))
			return EngineType.MYSQL;
		Paths.get(dbhome);
		return EngineType.EDB;
	}

    /**
     * 获取数据库类型
     * @return MYSQL或者EDB
     */
    public EngineType getEngineType() {
		return getEngineType(dbhome);
	}

	public static Zdb loadFromDb(String dbhome) throws Exception {
		Zdb zdb = null;
		switch (getEngineType(dbhome)) {
		case MYSQL:
			try (Connection conn = DriverManager.getConnection(dbhome)) {
				try (Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("SELECT value FROM _meta_ WHERE id=0")) {
					if (!rs.next())
						return null;
					zdb = new Zdb(new Naming.Root(), XMLUtils.getRootElement(rs.getBlob(1).getBinaryStream()));
				} catch (Exception e) {
					return null;
				}
			}
			break;
		case EDB:
			File file = new File(dbhome, "meta.xml");
			if (!file.exists())
				return null;
			zdb = new Zdb(new Naming.Root(), XMLUtils.getRootElement(file));
			break;
		}
		zdb.dbhome = dbhome;
		compile(zdb);
		return zdb;
	}

	private static void compile(Zdb zdb) throws Exception {
		Collection<Naming> unresoloved = zdb.getRoot().compile();
		if (!unresoloved.isEmpty()) {
			Trace.error(" Unresolved symobls:");
			unresoloved.forEach(Trace::error);
			throw new Exception("has unresolved symobls");
		}
	}

	private Zdb(Root root, Element self) throws Exception {
		super(root, self);
		initialize(self);
	}

	public Zdb(Project parent, Element self) throws Exception {
		super(parent, self);
		initialize(self);
	}

    /**
     * 读取XML中的ZDB配置信息
     * @param self xmlNode
     */
    public void initialize(Element self) {
		ElementHelper eh = new ElementHelper(self);
		dbhome = eh.getString("dbhome", "zdb");
		trnhome = eh.getString("trnhome");
		jdbcPoolSize = eh.getInt("jdbcPoolSize", jdbcPoolSize);
		defaultTableCache = eh.getString("defaultTableCache", defaultTableCache);
		zdbVerify = eh.getBoolean("zdbVerify", false);
		autoKeyInitValue = eh.getInt("autoKeyInitValue", autoKeyInitValue);
		autoKeyStep = eh.getInt("autoKeyStep", autoKeyStep);
		corePoolSize = eh.getInt("corePoolSize", corePoolSize);
		procPoolSize = eh.getInt("procPoolSize", procPoolSize);
		schedPoolSize = eh.getInt("schedPoolSize", schedPoolSize);
		checkpointPeriod = eh.getInt("checkpointPeriod", checkpointPeriod);
		luciferPeriod = eh.getInt("luciferPeriod", luciferPeriod);
		snapshotFatalTime = eh.getLong("snapshotFatalTime", snapshotFatalTime);
		marshalPeriod = eh.getInt("marshalPeriod", marshalPeriod);
		marshalN = eh.getInt("marshalN", marshalN);
		edbCacheSize = eh.getInt("edbCacheSize", edbCacheSize);
		edbLoggerPages = eh.getInt("edbLoggerPages", edbLoggerPages);
		enableReadLock = eh.getBoolean("enableReadLock", enableReadLock);
		eh.warnUnused("xml:base", "name");
	}

	public static final class Builder {
		Zdb zdb;

		public Builder(Root root) {
			zdb = new Zdb(root);
		}

		public Zdb build() {
			return zdb;
		}

		public Builder defaultTableCache(String defaultTableCache) {
			zdb.defaultTableCache = defaultTableCache;
			return this;
		}

		public Builder zdbVerify(boolean zdbVerify) {
			zdb.zdbVerify = zdbVerify;
			return this;
		}

		public Builder autoKeyInitValue(int autoKeyInitValue) {
			zdb.autoKeyInitValue = autoKeyInitValue;
			return this;
		}

		public Builder autoKeyStep(int autoKeyStep) {
			zdb.autoKeyStep = autoKeyStep;
			return this;
		}

		public Builder corePoolSize(int corePoolSize) {
			zdb.corePoolSize = corePoolSize;
			return this;
		}

		public Builder procPoolSize(int procPoolSize) {
			zdb.procPoolSize = procPoolSize;
			return this;
		}

		public Builder schedPoolSize(int schedPoolSize) {
			zdb.schedPoolSize = schedPoolSize;
			return this;
		}

		public Builder checkpointPeriod(int checkpointPeriod) {
			zdb.checkpointPeriod = checkpointPeriod;
			return this;
		}

		public Builder luciferPeriod(int luciferPeriod) {
			zdb.luciferPeriod = luciferPeriod;
			return this;
		}

		public Builder snapshotFatalTime(int snapshotFatalTime) {
			zdb.snapshotFatalTime = snapshotFatalTime;
			return this;
		}

		public Builder marshalPeriod(int marshalPeriod) {
			zdb.marshalPeriod = marshalPeriod;
			return this;
		}

		public Builder marshalN(int marshalN) {
			zdb.marshalN = marshalN;
			return this;
		}

		public Builder edbCacheSize(int edbCacheSize) {
			zdb.edbCacheSize = edbCacheSize;
			return this;
		}

		public Builder edbLoggerPages(int edbLoggerPages) {
			zdb.edbLoggerPages = edbLoggerPages;
			return this;
		}

		public Builder enableReadLock(boolean enableReadLock) {
			zdb.enableReadLock = enableReadLock;
			return this;
		}
	}

	private Zdb(Root root) {
		super(root, "");
	}

	@Override
	public boolean resolve() {
		if (!super.resolve())
			return false;
		List<Procedure> procedures = getChildren(Procedure.class);
		if (procedures.size() > 1) {
			throw new RuntimeException("procedure.size > 1");
		} else if (procedures.isEmpty()) {
			new Procedure(this).resolved();
		}
		return true;
	}

	public Procedure getProcedure() {
		return getChildren(Procedure.class).get(0);
	}

	public List<Variable> getDescendantVariables() {
		return getDescendants(Variable.class);
	}

	public List<Xbean> getXbeans() {
		return getChildren(Xbean.class);
	}

	public List<Cbean> getCbeans() {
		return getChildren(Cbean.class);
	}

	public List<Table> getTables() {
		return getChildren(Table.class);
	}

	public Table getTable(String name) {
		return getChild(Table.class, name);
	}

	public Cbean getCbean(String name) {
		return getChild(Cbean.class, name);
	}

	public Xbean getXbean(String name) {
		return getChild(Xbean.class, name);
	}

	public int getJdbcPoolSize() {
		return jdbcPoolSize;
	}

    /**
     * 获取数据库类型
     * @return 数据库连接字符串
     */
    public String getDbHome() {
		return dbhome;
	}

	public String getTrnHome() {
		return trnhome == null ? "" : trnhome;
	}

	public int getAutoKeyInitValue() {
		return autoKeyInitValue;
	}

	public int getAutoKeyStep() {
		return autoKeyStep;
	}

	public boolean isZdbVerify() {
		return zdbVerify;
	}

	public int getSchedPoolSize() {
		return schedPoolSize;
	}

	public int getCorePoolSize() {
		return corePoolSize;
	}

	public int getProcPoolSize() {
		return procPoolSize;
	}

	public int getLuciferPeriod() {
		return luciferPeriod;
	}

	public long getSnapshotFatalTime() {
		return snapshotFatalTime;
	}

	public int getMarshalN() {
		return marshalN;
	}

	public int getMarshalPeriod() {
		return marshalPeriod;
	}

	public int getCheckpointPeriod() {
		return checkpointPeriod;
	}

	public String getDefaultTableCache() {
		return defaultTableCache;
	}

	public int getEdbCacheSize() {
		return edbCacheSize;
	}

	public int getEdbLoggerPages() {
		return edbLoggerPages;
	}

	public boolean isEnableReadLock() {
		return enableReadLock;
	}

	public void setDbHome(String dbhome) {
		this.dbhome = dbhome;
	}

}
