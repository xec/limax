package limax.xmlgen.java;

import java.io.File;
import java.io.PrintStream;
import java.util.List;

import limax.xmlgen.Cbean;
import limax.xmlgen.Enum;
import limax.xmlgen.Main;
import limax.xmlgen.Variable;

class CbeanFormatter {

	static void make(Cbean cbean, File genDir) {
		boolean noverifySaved = Main.zdbNoverify;
		Main.zdbNoverify = true;
		_make(cbean, genDir);
		Main.zdbNoverify = noverifySaved;
	}

	private static void _make(Cbean cbean, File genDir) {
		String classname = cbean.getLastName();

		try (PrintStream ps = Zdbgen.openCBeanFile(genDir, classname)) {
			List<Variable> variables = cbean.getVariables();
			List<Enum> enums = cbean.getEnums();
			ps.println();
			ps.println("package cbean;");
			ps.println();
			ps.println("public class " + classname + " implements limax.codec.Marshal, "
					+ (cbean.attachment() == null ? "" : "limax.codec.StringMarshal, ") + "Comparable<" + classname
					+ "> {");
			ps.println();
			Declare.make(enums, variables, Declare.Type.PRIVATE, ps, "	");
			Construct.make(cbean, ps, "	");
			ConstructWithParam.make(variables, classname, ps, "	");
			variables.forEach(var -> VarGetter.make(var, ps, "	"));
			Marshal.make(cbean, ps, "	");
			if (cbean.attachment() != null)
				StringMarshal.make(cbean, ps, "	");
			Unmarshal.make(cbean, ps, "	");
			CompareTo.make(cbean, ps, "	");
			Equals.make(cbean, ps, "	");
			Hashcode.make(cbean, ps, "	");
			ps.println("}");
		}
	}
}
