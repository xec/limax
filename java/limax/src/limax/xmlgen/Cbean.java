package limax.xmlgen;

import java.util.List;
import java.util.Set;

import org.w3c.dom.Element;

import limax.util.ElementHelper;

public final class Cbean extends Type {
	private String comment;

	public Cbean(Zdb parent, Element self) throws Exception {
		super(parent, self);
		initialize(self);
	}

	public Cbean(Namespace parent, Element self) throws Exception {
		super(parent, self);
		initialize(self);
	}

	private void initialize(Element self) {
		Variable.verifyName(this);
		comment = Bean.extractComment(self);
		new ElementHelper(self).warnUnused("name");
	}

	public Cbean(Zdb parent, String name) {
		super(parent, name);
	}

	@Override
	boolean resolve() {
		if (!super.resolve())
			return false;
		if (!isConstType())
			throw new RuntimeException(getFullName() + " is not constant bean. only immutable variable is available");
		return true;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void depends(Set<Type> incls) {
		if (incls.add(this))
			getVariables().forEach(var -> var.getType().depends(incls));
	}

	public String getComment() {
		return comment;
	}

	public List<Variable> getVariables() {
		return getChildren(Variable.class);
	}

	public Variable getVariable(String varname) {
		return getChild(Variable.class, varname);
	}

	public List<Enum> getEnums() {
		return getChildren(Enum.class);
	}

	public String getLastName() {
		return getName();
	}

	public String getFirstName() {
		return "cbean";
	}

	public String getFullName() {
		return getFirstName() + "." + getName();
	}
}
