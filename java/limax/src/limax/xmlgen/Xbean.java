package limax.xmlgen;

import java.util.List;
import java.util.Set;

import org.w3c.dom.Element;

import limax.util.ElementHelper;

public class Xbean extends Type {

	private String comment;

	public Xbean(Zdb parent, Element self) throws Exception {
		super(parent, self);
		Variable.verifyName(this);
		ElementHelper eh = new ElementHelper(self);
		eh.warnUnused("name");
		comment = Bean.extractComment(self);
	}

	public Xbean(Zdb parent, String name) {
		super(parent, name);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void depends(Set<Type> incls) {
		if (incls.add(this))
			getVariables().forEach(var -> var.getType().depends(incls));
	}

	public String getComment() {
		return comment;
	}

	public List<Variable> getVariables() {
		return getChildren(Variable.class);
	}

	public Variable getVariable(String varname) {
		return getVariables().stream().filter(var -> var.getName().equals(varname)).findAny().orElse(null);
	}

	public List<Enum> getEnums() {
		return getChildren(Enum.class);
	}

	public String getLastName() {
		return getName();
	}

	public String getFirstName() {
		return "xbean";
	}

	public String getFullName() {
		return getFirstName() + "." + getName();
	}

	@Override
	public boolean isConstType() {
		return false;
	}
}
