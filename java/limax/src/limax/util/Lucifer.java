package limax.util;

import java.lang.management.LockInfo;
import java.lang.management.ManagementFactory;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

final class Lucifer implements Runnable {
	private final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private final ScheduledExecutorService scheduler;
	private final Random random = new Random(System.currentTimeMillis());
	private final ThreadGroup rootThreadGroup;
	private volatile long interruptCount = 0;
	private long sleepIdleMs = 1000;
	private final long period;

	private static ThreadGroup getRootThreadGroup() {
		ThreadGroup root = Thread.currentThread().getThreadGroup();
		for (ThreadGroup p = root.getParent(); null != p; p = p.getParent())
			root = p;
		return root;
	}

	private static Map<Long, Thread> enumerate(ThreadGroup group, boolean recurse) {
		Thread[] threads = new Thread[4096];
		int size;
		while ((size = group.enumerate(threads, recurse)) == threads.length)
			threads = new Thread[threads.length * 2];
		Map<Long, Thread> m = new HashMap<Long, Thread>();
		for (int i = 0; i < size; ++i) {
			Thread thread = threads[i];
			if (null != thread)
				m.put(thread.getId(), thread);
		}
		return m;
	}

	public long getInterruptCount() {
		return interruptCount;
	}

	public Lucifer(long period) {
		this.scheduler = ConcurrentEnvironment.getInstance().newScheduledThreadPool("Limax Lucifer Scheduler", 1, true);
		this.period = Math.max(period, 250);
		this.rootThreadGroup = getRootThreadGroup();
		scheduler.schedule(this, sleepIdleMs, TimeUnit.MILLISECONDS);
	}

	@Override
	public void run() {
		try {
			if (detect()) {
				sleepIdleMs = 1000;
			} else {
				sleepIdleMs += 1000;
				if (sleepIdleMs > period)
					sleepIdleMs = period;
			}
		} catch (Throwable ex) {
			if (Trace.isErrorEnabled())
				Trace.error("lucifer run " + ex);
		} finally {
			scheduler.schedule(this, sleepIdleMs, TimeUnit.MILLISECONDS);
		}
	}

	private final static int MAXDEPTH = 255;

	private boolean detect() {
		long[] deadlockedThreadIds = threadMXBean.findDeadlockedThreads();
		if (null == deadlockedThreadIds)
			return false;
		Map<Long, ThreadInfo> deadlockedThreads = new HashMap<Long, ThreadInfo>();
		for (ThreadInfo tinfo : threadMXBean.getThreadInfo(deadlockedThreadIds,
				threadMXBean.isObjectMonitorUsageSupported(), threadMXBean.isSynchronizerUsageSupported())) {
			try {
				if (null != tinfo && tinfo.getLockOwnerId() != -1)
					deadlockedThreads.put(tinfo.getThreadId(), tinfo);
			} catch (Throwable e) {
				if (Trace.isInfoEnabled())
					Trace.info("lucifer critical exeption");
			}
		}
		Map<Long, Thread> allThreads = null;
		while (!deadlockedThreads.isEmpty()) {
			Map<Long, ThreadInfo> cycle = new LinkedHashMap<Long, ThreadInfo>();
			ThreadInfo tinfo = deadlockedThreads.entrySet().iterator().next().getValue();
			do {
				if (null != cycle.put(tinfo.getThreadId(), tinfo)) {
					ThreadInfo infos[] = cycle.values().toArray(new ThreadInfo[0]);
					ThreadInfo victim = infos[random.nextInt(infos.length)];
					StringBuilder sb = new StringBuilder("Lucifer.interrupt thread \"").append(victim.getThreadName())
							.append("\" Id=").append(victim.getThreadId()).append(" in cycle:\n");
					for (ThreadInfo info : infos)
						dumpThreadInfoTo(info, sb);
					Trace.fatal(sb);
					allThreads = interrupt(victim, allThreads);
					break;
				}
			} while ((tinfo = deadlockedThreads.get(tinfo.getLockOwnerId())) != null);
			deadlockedThreads.keySet().removeAll(cycle.keySet());
		}
		return true;
	}

	private void dumpThreadInfoTo(ThreadInfo tinfo, StringBuilder sb) {
		sb.append("\"" + tinfo.getThreadName() + "\"" + " Id=" + tinfo.getThreadId() + " " + tinfo.getThreadState());
		if (tinfo.getLockName() != null)
			sb.append(" on " + tinfo.getLockName());
		if (tinfo.getLockOwnerName() != null)
			sb.append(" owned by \"" + tinfo.getLockOwnerName() + "\" Id=" + tinfo.getLockOwnerId());
		if (tinfo.isSuspended())
			sb.append(" (suspended)");
		if (tinfo.isInNative())
			sb.append(" (in native)");
		sb.append('\n');
		StackTraceElement[] stackTrace = tinfo.getStackTrace();
		int i = 0;
		for (; i < stackTrace.length && i < MAXDEPTH; i++) {
			StackTraceElement ste = stackTrace[i];
			sb.append("\tat " + ste.toString() + "\n");
			if (i == 0 && tinfo.getLockInfo() != null) {
				Thread.State ts = tinfo.getThreadState();
				switch (ts) {
				case BLOCKED:
					sb.append("\t-  blocked on " + tinfo.getLockInfo() + "\n");
					break;
				case WAITING:
					sb.append("\t-  waiting on " + tinfo.getLockInfo() + "\n");
					break;
				case TIMED_WAITING:
					sb.append("\t-  waiting on " + tinfo.getLockInfo() + "\n");
					break;
				default:
				}
			}

			for (MonitorInfo mi : tinfo.getLockedMonitors())
				if (mi.getLockedStackDepth() == i)
					sb.append("\t-  locked " + mi + "\n");
		}
		if (i < stackTrace.length)
			sb.append("\t...\n");

		LockInfo[] locks = tinfo.getLockedSynchronizers();
		if (locks.length > 0) {
			sb.append("\n\tNumber of locked synchronizers = " + locks.length + "\n");
			for (LockInfo li : locks)
				sb.append("\t- " + li + "\n");
		}
		sb.append('\n');
	}

	private final Map<Long, Thread> interrupt(ThreadInfo tinfo, Map<Long, Thread> allThreads) {
		try {
			ConcurrentEnvironment.Worker worker = ConcurrentEnvironment.Worker.get(tinfo.getThreadId());
			if (worker != null) {
				worker.luciferInterrupt();
				interruptCount++;
				return allThreads;
			}
			if (null == allThreads)
				allThreads = enumerate(rootThreadGroup, true);
			Thread thread = allThreads.get(tinfo.getThreadId());
			if (null != thread) {
				thread.interrupt();
				interruptCount++;
				return allThreads;
			}
			if (Trace.isInfoEnabled())
				Trace.info("Lucifer.interrupt: thread not found. " + tinfo);

		} catch (Throwable e) {
			Trace.fatal("Lucifer.interrupt " + tinfo, e);
		}
		return allThreads;
	}
}
