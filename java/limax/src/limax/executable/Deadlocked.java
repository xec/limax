package limax.executable;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

class Deadlocked extends JmxTool {

	@Override
	public void build(Options options) {
		options.add(Jmxc.options());
	}

	@Override
	public void run(Options options) throws Exception {
		Jmxc jmxc = Jmxc.connect(options);
		try {
			ThreadMXBean threadMBean = ManagementFactory.newPlatformMXBeanProxy(jmxc.mbeanServer(),
					ManagementFactory.THREAD_MXBEAN_NAME, ThreadMXBean.class);
			long[] threads = threadMBean.findDeadlockedThreads();
			System.out.println("deadlocked = " + (threads != null && threads.length > 0));
		} finally {
			jmxc.close();
		}
	}

}
