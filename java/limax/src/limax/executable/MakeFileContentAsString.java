package limax.executable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

class MakeFileContentAsString {
	private MakeFileContentAsString() {
	}

	public static void main(String args[]) throws Exception {
		if (args.length < 4) {
			System.out.println("usage : inputfile outputfile stringprefix stringsubfix");
			return;
		}
		try (final BufferedReader br = new BufferedReader(new FileReader(args[0]));
				final BufferedWriter bw = new BufferedWriter(new FileWriter(args[1]))) {

			bw.write(args[2]);
			bw.newLine();
			String line = br.readLine();
			while (null != line) {
				line = line.replace("\"", "\\\"");
				bw.write("\"");
				bw.write(line);
				bw.write("\\n\"");
				bw.newLine();
				line = br.readLine();
			}
			bw.write(args[3]);
		}
	}
}
