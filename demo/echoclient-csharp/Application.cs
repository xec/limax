﻿using echo.echoclient.echoviews;
using echo.echoviews;
using limax.codec;
using limax.endpoint;
using limax.net;
using limax.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace echoclient_csharp
{
    class Application : EndpointListener
    {
        private EndpointManager endpointManager = null;
        private TaskScheduler scheduler = null;
        private static Application instance;

        static void Main(string[] args)
        {
            Application app = new Application();

            app.ConnectAndAuth();

            while (true)
            {
                string str = Console.ReadLine();
                EchoMessage.getInstance().sendMessage(str);
            }
        }

        public Application()
        {
            instance = this;
            Endpoint.openEngine();
#if DEBUG
            Trace.open((str) => { Console.WriteLine(str); }, Trace.Level.Debug);
#endif
        }

        public static Application getInstance()
        {
            return instance;
        }

        internal Executor getExecutor()
        {
            return a =>
            {
                //Utils.runOnUiThread(a); 
                //Task t = new Task(Utils.uiThreadSchedule); 
                //t.Start(scheduler); 
                a();
            };
        }

        public static EndpointManager getEndpointManager()
        {
            return instance.endpointManager;
        }


        public void ConnectAndAuth()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            if (null == scheduler)
                scheduler = TaskScheduler.FromCurrentSynchronizationContext();

            var config = Endpoint.createLoginConfigBuilder("127.0.0.1", 10000, "1111", "123456", "test")
                    .staticViewClasses(ViewManager.createInstance(200))
                //.executor(getExecutor())
                    .build();

            Endpoint.start(config, this);
        }

        public void onSocketConnected()
        {
            Trace.info("onSocketConnected");
        }

        public void onKeyExchangeDone()
        {
            Trace.info("onKeyExchangeDone");
        }

        public void onKeepAlived(int ms)
        {
            Trace.info("keepalive: " + ms);
        }

        public void onErrorOccured(int source, int code, Exception exception)
        {
            Trace.error("onErrorOccured " + source + " " + code + " exception = " + exception);
        }

        public void onAbort(Transport transport)
        {
            Trace.error("OnAbort" + transport);
        }

        public void onManagerInitialized(Manager manager, Config config)
        {
            Trace.info("onManagerInitialized");
            this.endpointManager = (EndpointManager)manager;
        }

        public void onManagerUninitialized(Manager manager)
        {
            Trace.info("Uninitialized");
        }

        public void onTransportAdded(Transport transport)
        {
            Trace.info("onTransportAdded" + transport);

            //EchoMessage.getInstance().registerListener("response", e =>
            //{
            //    var msg = (ChatMessage)e.value;
            //    Trace.info(String.Format("session {0} say: {1}", msg.user, msg.msg));
            //});

            //EchoMessage.getInstance().registerListener("test", e =>
            //{
            //    var msg = (Octets)e.value;
            //    var d = msg.getBytes();
            //    Console.WriteLine("length: {0}", d.Length);
            //});
        }

        public void onTransportRemoved(Transport transport)
        {
            Trace.info("onTransportRemoved" + transport);
        }
    }
}
