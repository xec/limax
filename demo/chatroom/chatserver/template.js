var providers = [100];
var limax = Limax(function(ctx) {
ctx.onerror = function(e) {
	console.error('limax error', e);
}
ctx.onclose = function(e) {
	console.log('limax close', e);
}
ctx.onopen = function() {
	var type = ['NEW', 'REPLACE', 'TOUCH', 'DELETE' ];
	var v100 = ctx[100];
	v100.chatviews.CommonInfo.onchange = function(e) {
		console.log("v100.chatviews.CommonInfo.onchange", e.view, e.sessionid, e.fieldname, e.value, type[e.type]);
	}
	v100.chatviews.UserInfo.onchange = function(e) {
		console.log("v100.chatviews.UserInfo.onchange", e.view, e.sessionid, e.fieldname, e.value, type[e.type]);
	}
	v100.chatviews.ChatRoom.onchange = function(e) {
		console.log("v100.chatviews.ChatRoom.onchange", e.view, e.sessionid, e.fieldname, e.value, type[e.type]);
	}
	v100.chatviews.ChatRoom.onopen = function(instanceid, memberids) {
		this[instanceid].onchange = this.onchange;
		console.log("v100.chatviews.ChatRoom.onopen", this[instanceid], instanceid, memberids);
	}
	v100.chatviews.ChatRoom.onattach = function(instanceid, memberid) {
		console.log("v100.chatviews.ChatRoom.onattach", this, instanceid, memberid);
	}
	v100.chatviews.ChatRoom.ondetach = function(instanceid, memberid, reason) {
		console.log("v100.chatviews.ChatRoom.ondetach", this, instanceid, memberid, reason);
	}
	v100.chatviews.ChatRoom.onclose = function(instanceid) {
		console.log("v100.chatviews.ChatRoom.onclose", this, instanceid);
	}
}});
var login = {
	scheme : 'ws',
	host : '127.0.0.1',
	username : '',
	token : '',
	platflag : '',
	pvids : [100],
}
var connector = WebSocketConnector(limax, login);
