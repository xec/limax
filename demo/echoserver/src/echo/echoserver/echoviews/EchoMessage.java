package echo.echoserver.echoviews;
import limax.provider.ProcedureHelper;
import limax.provider.SessionView;
import limax.util.Pair;
import limax.zdb.Procedure;
import xbean.UserInfo;

public final class EchoMessage extends echo.echoserver.echoviews._EchoMessage {

    private EchoMessage(SessionView.CreateParameter param) {
        super(param);
        // bind here
    }

    @Override
    protected void onClose() {
    }

    @Override
    protected void onMessage(String message, long sessionid) {
        //Trace.warn("on EchoMesage " + message + " with " + sessionid);
        System.out.printf("[%10d]: %s\n", sessionid, message);

        Procedure.submit(()->{
            Pair<Long, UserInfo> info = table.Userinfo.insert();
            info.getValue().setNickname(message);


            Procedure.execute(()->{
                System.out.println("begin walk");
                table.Userinfo.get().walk((id, value) -> {
                    System.out.println("dump walk");
                    System.out.println(value.getNickname());


                    return true;
                });
                System.out.println("end walk");
                return true;
            });

            System.out.println("insert complete");
            System.out.println("submit begin");
            return false;
        });



        System.out.println("on message complete");
    }

}

