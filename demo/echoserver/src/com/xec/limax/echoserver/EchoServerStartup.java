package com.xec.limax.echoserver;

import limax.xmlconfig.Service;

public class EchoServerStartup {
	public static void main(String[] args) throws Exception {
		System.out.println("Server startup");
		Service.run("service-echoserver.xml");
	}
}
