package com.xec.limax.echoserver;

import java.io.IOException;

import limax.net.Config;
import limax.net.Manager;
import limax.net.ServerManager;
import limax.net.Transport;
import limax.provider.*;
import limax.util.Trace;

public class SessionManager implements ProviderListener {
	static private final SessionManager instance = new SessionManager();
	private ServerManager manager = null;

	public ServerManager getManager() {
		return manager;
	}

	public static SessionManager getInstance() {
		return instance;
	}

	@Override
	public void onManagerInitialized(Manager manager, Config config) {
		// TODO Auto-generated method stub
		this.manager = (ServerManager)manager;

		try {
			getManager().openListen();
		} catch (IOException e) {
			Trace.error(e);
		}
	}

	@Override
	public void onManagerUninitialized(Manager manager) {
		// TODO Auto-generated method stub
		this.manager = null;
	}
	
	@Override
	public void onTransportAdded(Transport transport) throws Exception {
		if (Trace.isInfoEnabled())
			Trace.info("onTransportAdded " + transport);
	}

	@Override
	public void onTransportRemoved(Transport transport) throws Exception {
		if (Trace.isInfoEnabled())
			Trace.info("onTransportRemoved " + transport);
	}

	@Override
	public void onTransportDuplicate(Transport transport) throws Exception {
		if (Trace.isInfoEnabled())
			Trace.info("onTransportDuplicate " + transport);
		manager.close(transport);
	}

}
