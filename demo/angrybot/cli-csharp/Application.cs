﻿using angrybot.angrybotclient.angrybotviews;
using limax.endpoint;
using limax.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cli_csharp
{
    class Application : EndpointListener
    {
        private string username;
        public static EndpointManager endpointManager = null;
        static void Main(string[] args)
        {
            Application app = new Application(args[0]);
            app.exec();

            // only for wait
            Console.ReadLine();
        }

        public static EndpointManager getManager()
        {
            return endpointManager;
        }

        public Application(string username)
        {
            this.username = username;
            Endpoint.openEngine();
#if DEBUG
            Trace.open((str) => { Console.WriteLine(str); }, Trace.Level.Debug);
#endif
        }

        public void exec()
        {
            Trace.info("username=" + username);
            var config = Endpoint.createLoginConfigBuilder("127.0.0.1", 10000, this.username, "123456", "test")
                    .staticViewClasses(ViewManager.createInstance(100))
                    .build();

            Endpoint.start(config, this);
        }

        public void onSocketConnected()
        {
            Trace.info("onSocketConnected");
        }

        public void onKeyExchangeDone()
        {
            Trace.info("onKeyExchangeDone");
        }

        public void onKeepAlived(int ms)
        {
            Trace.info("onKeepAlived:" + ms);
        }

        public void onErrorOccured(int source, int code, Exception exception)
        {
            Trace.info("onErrorOccured:" + code);
        }

        public void onAbort(limax.net.Transport transport)
        {
            Trace.info("onAbort");
        }

        public void onManagerInitialized(limax.net.Manager manager, limax.net.Config config)
        {
            Trace.info("onManagerInitialized");
            Application.endpointManager = (EndpointManager)manager;
        }

        public void onManagerUninitialized(limax.net.Manager manager)
        {
            Trace.info("onManagerUninitialized");
        }

        public void onTransportAdded(limax.net.Transport transport)
        {
            Trace.info("onTransportAdded");

            //TestProtocol tp = new TestProtocol(10, 20);
            //tp.send(transport);

            HallInfo.getInstance().registerListener("rooms",
                e => UpdateRooms((List<angrybot.angrybotviews.GameRoomInfo>)e.value));
        }

        public void onTransportRemoved(limax.net.Transport transport)
        {
            Trace.info("onTransportRemoved");
        }

        public void UpdateRooms(List<angrybot.angrybotviews.GameRoomInfo> rooms)
        {
            // 这里得到大厅列表
            // 测试程序只加入第一个大厅
            UserInfo.getInstance().Join("test room id");

            
        }
    }
}
