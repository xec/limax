using System;
using System.Collections.Generic;
using limax.codec;
using limax.util;
using angrybot.angrybotviews;
using System.Threading;

namespace angrybot.angrybotclient.angrybotviews
{
	public sealed partial class GameRoom
	{
        protected void ThreadProc1()
        {
            for (; ; )
            {
                UserInfo.getInstance().Move(10, -200, 200);
                //UserInfo.getInstance().Rotate(10.00000000001f, -210.00000000001f, 3010.00000000001f, -1.00000001f);
                UserInfo.getInstance().Fire(true);
            }

        }

        protected void ThreadProc2()
        {
            for (; ; )
            {
                //UserInfo.getInstance().Move(10.00000000001f, -210.00000000001f, 3010.00000000001f);
                UserInfo.getInstance().Rotate(200, -200, 200, -200);
                UserInfo.getInstance().Fire(true);
            }

        }

        protected void ThreadProc3()
        {
            for (; ; )
            {
                UserInfo.getInstance().Move(200, -200, 200);
                UserInfo.getInstance().Rotate(200, -200, 200, -200);
                UserInfo.getInstance().Fire(true);
            }

        }
		override protected void onClose() {
            Trace.warn("on close");
        }
		override protected void onAttach(long sessionid) {
            Trace.warn("on attach");

            // OtherPlayer = GameObject.Instant("");
            // OtherPlayer.PlayerID = sessionid;

        }
		override protected void onDetach(long sessionid, byte reason) {
            Trace.warn("on detach");
        }
		override protected void onOpen(ICollection<long> sessionids) {
            Trace.warn("on open");

            // Create GameObject of player is mine.
            // Player = GameObject.Instant();
            // Player.PlayerID = Application.GetManager().getSessionId();

            registerListener("user_move", e => UpdateUserTransform(e.sessionid, (TransformStruct)e.value));
            registerListener("user_fire", e => {
                Console.WriteLine("OnUserFire: " + e.value);
                UserInfo.getInstance().Fire(true);
            });

            //new Thread(new ThreadStart(ThreadProc1)).Start();
            //new Thread(new ThreadStart(ThreadProc2)).Start();
            //new Thread(new ThreadStart(ThreadProc3)).Start();
            for (; ; )
            {
                UserInfo.getInstance().Move(200, -200, 200);
                UserInfo.getInstance().Rotate(200, -200, 200, -200);
                UserInfo.getInstance().Fire(true);
            }
            //UserInfo.getInstance().Fire(true);
        }

        public void UpdateUserTransform(long session_id, TransformStruct transform)
        {
            Console.WriteLine(
                "session {0} move {1}, {2}, {3}",
                session_id,
                transform.position.x,
                transform.position.y,
                transform.position.z);


            //UserInfo.getInstance().Move(200, -200, 200);
            //UserInfo.getInstance().Rotate(200, -200, 200, -200);
            //UserInfo.getInstance().Fire(true);
        }
	}
}
