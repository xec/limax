
package angrybot.angrybotserver.angrybotviews;
import com.xec.angrybot.SessionManager;
import angrybot.angrybotviews.TransformStruct;
import limax.provider.SessionView;
import limax.util.Trace;
import table.Userinfo;

public final class UserInfo extends angrybot.angrybotserver.angrybotviews._UserInfo {
	private TransformStruct transform = new TransformStruct();

	private UserInfo(SessionView.CreateParameter param) {
		super(param);
		// bind here
		
		Trace.warn("Create UserInfo");
		Trace.warn("Sync HallInfo to client");
		HallInfo.getInstance().syncToClient(getSessionId());
	}

	@Override
	protected void onClose() {
	}

	@Override
	protected void onMessage(String message, long sessionid) {
		Trace.warn("UserInfo get message: " + message);
	}

	@Override
	protected void onControl(Move param, long sessionid) {
		//Trace.warn(sessionid + " on control move");
		transform.position.x = param.x;
		transform.position.y = param.y;
		transform.position.z = param.z;
		UserInfo.getInstance(sessionid).setTransform(transform);
	}

	@Override
	protected void onControl(Rotate param, long sessionid) {
		//Trace.warn(sessionid + " on control rotate");
		transform.rotation.x = param.x;
		transform.rotation.y = param.y;
		transform.rotation.z = param.z;
		transform.rotation.w = param.w;
		UserInfo.getInstance(sessionid).setTransform(transform);
	}

	@Override
	protected void onControl(Join param, long sessionid) {
		
		Trace.warn(sessionid + " on join room: " + param.room_id);
		SessionManager.getInstance().getDefaultGameRoom().getMembership().add(sessionid);
	}

	@Override
	protected void onControl(Leave param, long sessionid) {
		// TODO Not Implement
		Trace.warn(sessionid + " on leave room");
	}

	@Override
	protected void onControl(Fire param, long sessionid) {

		Trace.warn("On Player " + sessionid + " Fire: " + param.firing);
		UserInfo.getInstance(sessionid).setFire(param.firing);
	}

	@Override
	protected void onControl(Direction param, long sessionid) {
		//Trace.warn(sessionid + " on control direction");
		transform.direction.x = param.x;
		transform.direction.y = param.y;
		transform.direction.z = param.z;
		
		UserInfo.getInstance(sessionid).setTransform(transform);
		
	}

	@Override
	protected void onControl(Speed param, long sessionid) {
		//Trace.warn(sessionid + " on control speed");
		transform.speed.x = param.x;
		transform.speed.y = param.y;
		transform.speed.z = param.z;
		
		UserInfo.getInstance(sessionid).setTransform(transform);
	}

	@Override
	protected void onControl(MoveAction param, long sessionid) {
		transform.position = param.trans.position;
		transform.rotation = param.trans.rotation;
		transform.direction = param.trans.direction;
		transform.speed = param.trans.speed;

		UserInfo.getInstance(sessionid).setTransform(transform);
	}

}

