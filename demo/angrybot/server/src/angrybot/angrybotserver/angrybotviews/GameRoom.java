
package angrybot.angrybotserver.angrybotviews;

import limax.provider.TemporaryView.Membership.AbortReason;
import limax.util.Trace;
import limax.provider.TemporaryView;

public final class GameRoom extends angrybot.angrybotserver.angrybotviews._GameRoom {

	private GameRoom(TemporaryView.CreateParameter param) {
		super(param);
		// bind here
	}

	@Override
	protected void onClose() {
	}

	@Override
	protected void onAttachAbort(long sessionid, AbortReason reason) {
	}

	@Override
	protected void onDetachAbort(long sessionid, AbortReason reason) {
	}

	@Override
	protected void onAttached(long sessionid) {
	}

	@Override
	protected void onDetached(long sessionid, byte reason) {
		if (reason >= 0) {
			//Application reason
		} else {
			//Connection abort reason
		}
	}

	@Override
	protected void onMessage(String message, long sessionid) {
		Trace.warn("GameRoom get message" + message);
	}

}

