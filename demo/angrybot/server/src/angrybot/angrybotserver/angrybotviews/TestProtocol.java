
package angrybot.angrybotserver.angrybotviews;

// {{{ XMLGEN_IMPORT_BEGIN
// {{{ DO NOT EDIT THIS
/** Test Protocol **/
// DO NOT EDIT THIS }}}
// XMLGEN_IMPORT_END }}}

public class TestProtocol extends limax.net.Protocol {
	@Override
	public void process() {
		// protocol handle
	}

	// {{{ XMLGEN_DEFINE_BEGIN
	// {{{ DO NOT EDIT THIS
	public static int TYPE;

	public int getType() {
		return TYPE;
	}

    public float test_a; 
    public float test_b; 

	public TestProtocol() {
	}

	public TestProtocol(float _test_a_, float _test_b_) {
		this.test_a = _test_a_;
		this.test_b = _test_b_;
	}

	@Override
	public limax.codec.OctetsStream marshal(limax.codec.OctetsStream _os_) {
		_os_.marshal(this.test_a);
		_os_.marshal(this.test_b);
		return _os_;
	}

	@Override
	public limax.codec.OctetsStream unmarshal(limax.codec.OctetsStream _os_) throws limax.codec.MarshalException {
		this.test_a = _os_.unmarshal_float();
		this.test_b = _os_.unmarshal_float();
		return _os_;
	}

	@Override
	public String toString() {
		StringBuilder _sb_ = new StringBuilder(super.toString());
		_sb_.append("=(");
		_sb_.append(this.test_a).append(",");
		_sb_.append(this.test_b).append(",");
		_sb_.append(")");
		return _sb_.toString();
	}

	// DO NOT EDIT THIS }}}
	// XMLGEN_DEFINE_END }}}

}

