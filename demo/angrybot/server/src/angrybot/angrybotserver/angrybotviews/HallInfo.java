
package angrybot.angrybotserver.angrybotviews;

import limax.provider.GlobalView;
import limax.util.Trace;

public final class HallInfo extends angrybot.angrybotserver.angrybotviews._HallInfo {

	private HallInfo(GlobalView.CreateParameter param) {
		super(param);
		// bind here
	}

	@Override
	protected void onClose() {
	}

	@Override
	protected void onMessage(String message, long sessionid) {
		Trace.warn("hallInfo get message" + message);
	}

}

