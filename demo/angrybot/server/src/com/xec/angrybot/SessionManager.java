package com.xec.angrybot;

import java.io.IOException;
import java.util.ArrayList;

import angrybot.angrybotserver.angrybotviews.GameRoom;
import angrybot.angrybotserver.angrybotviews.HallInfo;
import angrybot.angrybotviews.GameRoomInfo;
import limax.net.Config;
import limax.net.Manager;
import limax.net.ServerManager;
import limax.net.Transport;
import limax.provider.ProviderListener;
import limax.util.Trace;

public class SessionManager implements ProviderListener {
	static private final SessionManager instance = new SessionManager();
	private ServerManager manager = null;
	
	// only for test
	public GameRoom defaultGameRoom = null;
	
	public static SessionManager getInstance() {
		return instance;
	}
	
	public ServerManager getManager() {
		return manager;
	}
	
	public GameRoom getDefaultGameRoom() {
		return defaultGameRoom;
	}

	
	@Override
	public void onManagerInitialized(Manager manager, Config config) {
		// TODO Auto-generated method stub
		this.manager = (ServerManager)manager;
		Trace.warn("onSessionManager ManagerInistialized");
		
		// initialize hall info
		final ArrayList<GameRoomInfo> rooms = new ArrayList<>();
		
		rooms.add(new GameRoomInfo("room1", "unq1"));
		rooms.add(new GameRoomInfo("room2", "unq2"));
		HallInfo.getInstance().setRooms(rooms);
		
		defaultGameRoom = GameRoom.createInstance();
		
		try {
			getManager().openListen();
		} catch (IOException e) {
			Trace.error(e);
		}
	}

	@Override
	public void onManagerUninitialized(Manager manager) {
		// TODO Auto-generated method stub
		this.manager = null;
	}

	@Override
	public void onTransportAdded(Transport transport) throws Exception {
		// TODO Auto-generated method stub
		Trace.info("on transport add");

	}

	@Override
	public void onTransportRemoved(Transport transport) throws Exception {
		// TODO Auto-generated method stub
		Trace.info("on transport remove");
		manager.close(transport);
	}

	@Override
	public void onTransportDuplicate(Transport transport) throws Exception {
		// TODO Auto-generated method stub
        Trace.info("on transport duplicate");

	}

}
