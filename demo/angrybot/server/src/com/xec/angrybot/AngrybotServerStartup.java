package com.xec.angrybot;
import limax.util.Trace;
import limax.xmlconfig.Service;

public class AngrybotServerStartup {

	public static void main(String[] args) throws Exception {
		
		Trace.openNew();
		Service.run("service-angrybotserver.xml");
	}
}
